# Prosjekt 2 - IMT2291 - Våren 2020

## How to start up the project

- Clone the repo:

```
$ git clone https://bitbucket.org/army_boys/imt2291-prosjekt2-2020.git
```

- Start up docker
- Have node.js installed
- Run these commands within the folder

```
$ docker-compose up -d && cd front-end && npm install && npm start
```

## To start up the project on regular launches after install

```
$ docker-compose up -d && cd front-end && npm start
```

## To login wih pre created admin user, follow these steps:

- Login with this email `admin@admin.com`
- and with this password `admin`

## Documentation

Documentation found in the wiki page of the bitbucket repository
https://bitbucket.org/army_boys/imt2291-prosjekt2-2020/src/master/
