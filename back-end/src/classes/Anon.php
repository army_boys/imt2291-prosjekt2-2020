<?php

class Anon
{
  private $video = null;
  private $playlist = null;

  public function __construct($video, $playlist)
  {
    $this->video = $video;
    $this->playlist = $playlist;

    if (isset($_GET['search'])) {
      $this->search($_GET['search']);
    } elseif ($_GET['openPlaylist']) {
      $tmp = $this->playlist->getPlaylists(null, $_GET['openPlaylist']);
      if ($tmp['status'] === 'OK') {
        echo json_encode(array(
          'view' => 'openPlaylist',
          'playlist' => $tmp['playlist'],
          'videos' => $tmp['videos'],
        ));
      } else {
        echo json_encode($tmp);
      }
    } elseif (isset($_GET['newVideos'])) {
      $tmp = $this->video->getVideos(null, null, $_GET['newVideos']);
      if ($tmp['status'] === 'OK') {
        echo json_encode(array('videos' => $tmp['data']));
      } else {
        echo json_encode($tmp);
      }
    } elseif (isset($_GET['newPlaylists'])) {
      $tmp = $this->playlist->getPlaylists(
        null,
        null,
        false,
        $_GET['newPlaylists'],
      );
      if ($tmp['status'] === 'OK') {
        echo json_encode(array('playlists' => $tmp['data']));
      } else {
        echo json_encode($tmp);
      }
    } elseif (isset($_GET['randomVideos'])) {
      $tmp = $this->video->getVideos(null, null, $_GET['randomVideos']);
      if ($tmp['status'] === 'OK') {
        echo json_encode(array('videos' => $tmp['data']));
      } else {
        echo json_encode($tmp);
      }
    }
  }

  public function search($searchWord)
  {
    if (strlen($searchWord) > 2) {
      $vTmp = $this->video->search($searchWord);
      $displayArray = array();
      $displayArray['view'] = 'search';
      if ($vTmp['status'] === 'OK') {
        $displayArray['videos'] = $vTmp['data'];
        $videoLength = count($vTmp['data']);
        $displayArray['videoMessage'] =
          'Fant ' . $videoLength . $videoLength === 1 ? ' video' : ' videoer';
      } else {
        $displayArray['videoMessage'] =
          'Fant ingen videoer med dette søkeordet!';
      }
      $pTmp = $this->playlist->search($searchWord);

      if ($pTmp['status'] === 'OK') {
        $displayArray['playlists'] = $pTmp['data'];
        $playlistLength = count($pTmp['data']);
        $displayArray['playlistMessage'] =
          'Fant ' . $displayArray . $displayArray === 1
            ? ' spilleliste'
            : ' spillelister';
      } else {
        $displayArray['playlistMessage'] =
          'Fant ingen spillelister med dette søkeordet!';
      }
      echo json_encode($displayArray);
    } else {
      echo json_encode(array(
        'errorMessage' => 'Søkeordet må være større enn 2 bokstaver',
      ));
    }
  }
}

?>
