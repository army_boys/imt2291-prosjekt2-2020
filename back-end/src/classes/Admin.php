<?php

session_start();

class Admin
{
  private $user = null;

  public function __construct($user)
  {
    $this->user = $user;

    if (isset($_GET['viewUsers'])) {
      $this->viewUsers($_GET['viewUsers']);
    } elseif (isset($_POST['changeUser'])) {
      $this->changeUser($_POST['email']);
    } elseif (isset($_POST['giveAccess'])) {
      $this->giveAccess($_POST['email'], $_POST['type']);
    }
  }

  public function viewUsers($type)
  {
    $usersFromDB =
      $type == 'validate'
        ? $this->user->getUsers()
        : $this->user->getUsers('all', $_SESSION['userData']['email']);
    if ($usersFromDB['status'] === 'OK') {
      echo json_encode(['users' => $usersFromDB['data']]);
    } else {
      echo json_encode(['status' => 'FAIL']);
    }
  }

  public function changeUser($idOfUserToChange)
  {
    $userToChange = $this->user->updateUser($idOfUserToChange);
    if ($userToChange['status'] === 'OK') {
      echo json_encode(array(
        'showUsers' => 'validate',
        'users' => array(),
        'status' => 'OK',
      ));
    } else {
      echo json_encode($userToChange);
    }
  }

  public function giveAccess($idOfUserToGiveAccess, $type = 'admin')
  {
    $userToChange = $this->user->updateUser(
      $idOfUserToGiveAccess,
      false,
      $type,
    );
    if ($userToChange['status'] === 'OK') {
      echo json_encode(array(
        'showUsers' => 'all',
        'users' => array(),
        'status' => 'OK',
      ));
    } else {
      echo json_encode($userToChange);
    }
  }
}

?>
