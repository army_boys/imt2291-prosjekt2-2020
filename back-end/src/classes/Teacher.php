<?php

require_once 'Validator.php';

class Teacher
{
  private $video = null;
  private $playlist = null;

  /**
   * The construct function handles all the GET and POST requests
   * related to the user. And defines the private class variables.
   */
  public function __construct($video, $playlist)
  {
    $this->video = $video;
    $this->playlist = $playlist;

    if (isset($_GET['showYourVideos'])) {
      $tmp = $this->video->getVideos($_SESSION['userData']['username']);

      if ($tmp['status'] === 'OK') {
        echo json_encode(array(
          'videos' => $tmp['data'],
          'view' => 'showVideos',
        ));
      } else {
        echo json_encode(array(
          'message' => 'Du har ingen videoer ennå!',
          'view' => 'showVideos',
        ));
      }
    } elseif (isset($_GET['showYourPlaylists'])) {
      $tmp = $this->playlist->getPlaylists(
        $_SESSION['userData']['username'],
        null,
        false,
      );
      if ($tmp['status'] === 'OK') {
        echo json_encode(array(
          'status' => $tmp['status'],
          'playlists' => $tmp['data'],
          'view' => 'showPlaylists',
        ));
      } else {
        echo json_encode(array(
          'message' => 'Du har ingen spillelster ennå!',
          'status' => $tmp['status'],
        ));
      }
    } elseif (isset($_GET['editPlaylist'])) {
      $tmp = $this->playlist->getPlaylists(null, $_GET['editPlaylist']);
      if ($tmp['status'] === 'OK') {
        $otherVideos = $this->video->getOtherVideos($_GET['editPlaylist']);
        if ($otherVideos['status'] === 'OK') {
          echo json_encode(array(
            'view' => 'editPlaylist',
            'playlist' => $tmp['playlist'],
            'videos' => $tmp['videos'],
            'otherVideos' => $otherVideos['data'],
          ));
        } else {
          echo json_encode(array(
            'view' => 'editPlaylist',
            'playlist' => $tmp['playlist'],
            'videos' => $tmp['videos'],
            'message' => 'Det finnes ingen andre videoer...',
          ));
        }
      } else {
        echo json_encode(array('error' => $tmp));
      }
    } elseif (isset($_GET['deleteVideo'])) {
      $tmp = $this->video->deleteVideo(
        $_GET['deleteVideo'],
        $_SESSION['userData']['username'],
      );
      echo json_encode(array('title' => 'Video slettet'));
    } elseif (isset($_GET['deletePlaylist'])) {
      $tmp = $this->playlist->deletePlaylist(
        $_GET['deletePlaylist'],
        $_SESSION['userData']['username'],
      );
      echo json_encode(array('title' => 'Spilleliste slettet'));
    } elseif (isset($_GET['editVideo'])) {
      $tmp = $this->video->getVideos(null, $_GET['editVideo']);
      echo json_encode(array('view' => 'editVideo', 'video' => $tmp['data']));

      /* POST REQUESTS */
    } elseif (isset($_POST['addVideo'])) {
      $this->addVideo($_POST, $_FILES, $_SESSION['userData']['username']);
    } elseif (isset($_POST['addPlaylist'])) {
      $this->addPlaylist($_POST, $_SESSION['userData']['username']);
    } elseif (isset($_POST['savePlaylist'])) {
      $this->savePlaylist($_POST, $_SESSION['userData']['username']);
    } elseif (isset($_POST['changeVideo'])) {
      $this->changeVideo($_POST, $_FILES, $_SESSION['userData']['username']);
    }
  }

  /**
   * @param array $formData is all the data from the post request.
   * @param array $file is the file information from the post request.
   * @param string $username of the user adding the video.
   * This data is passed to the video class' createVideo and
   * prepareDataForInsertion function.
   */
  private function addVideo($formData, $file, $username)
  {
    $valid = Validator::validateVideoSubmit($formData, $file);
    if ($valid['status'] === 'OK') {
      $dataToSend = $this->video->prepareDataForInsertion(
        $formData,
        $file,
        $username,
      );

      $tmp = $this->video->createVideo($dataToSend);
      if ($tmp['status' === 'OK']) {
        echo json_encode(array(
          'status' => 'OK',
          'title' => 'Video lagt til',
          'message' => $dataToSend['name'] . " ble lagt til",
        ));
      } else {
        echo json_encode($tmp);
      }
    } else {
      echo json_encode($valid);
    }
  }

  /**
   * @param array $formData is all the data from the post request.
   * @param string $username of the user adding the Playlist.
   * This data is passed to the Playlist class' createPlaylist and
   * prepareDataForInsertion function.
   */
  private function addPlaylist($formData, $username)
  {
    $valid = Validator::validatePlaylistSubmit($formData);
    if ($valid['status'] === 'OK') {
      if (
        isset($formData['videosToAdd']) &&
        count($formData['videosToAdd']) >= 1
      ) {
        $videosToAdd = array();

        foreach ($formData['videosToAdd'] as $key => $video) {
          array_push($videosToAdd, array('videoId' => $video, 'rank' => $key));
        }
        $formData['videosToAdd'] = array();
        $formData['videosToAdd'] = $videosToAdd;
      }

      $dataToSend = $this->playlist->prepareDataForInsertion(
        $formData,
        $username,
      );

      $tmp = $this->playlist->createPlaylist($dataToSend);
      echo json_encode(array(
        'status' => 'OK',
        'title' => 'Spilleliste lagt til',
        'message' => $dataToSend['title'] . " ble lagt til",
      ));
    } else {
      echo json_encode($valid);
    }
  }

  /**
   * @param array $formData is all the data from the post request.
   * This data is passed to the Playlist class' updatePlaylist and
   * prepareDataForInsertion function.
   */
  private function savePlaylist($formData, $username)
  {
    if (isset($formData['videosToRemove'])) {
      foreach ($formData['videoIds'] as $key => $value) {
        if (in_array($value, $formData['videosToRemove'])) {
          unset($formData['videoIds'][$key]);
          unset($formData['videoOrder'][$key]);
        }
      }
      unset($formData['videosToRemove']);
    }

    $valid = Validator::validatePlaylistSubmit($formData);
    if ($valid['status'] === 'OK') {
      $dataToSend = $this->playlist->prepareDataForInsertion(
        $formData,
        $username,
      );
      $videosToReorder = array();
      $rank = 1;
      if (isset($formData['videoIds'])) {
        for ($x = 0; $x < count($formData['videoIds']); $x++) {
          $id = $formData['videoIds'][$x];
          $rank =
            $formData['videoOrder'][$x] === ''
              ? 10
              : $formData['videoOrder'][$x];
          array_push($videosToReorder, array(
            'videoId' => $id,
            'rank' => $rank,
          ));
          $rank = $rank + 1;
        }
      }
      if (
        isset($formData['videosToAdd']) &&
        count($formData['videosToAdd']) > 0
      ) {
        for ($x = 0; $x < count($formData['videosToAdd']); $x++) {
          $id = $formData['videosToAdd'][$x];

          array_push($videosToReorder, array(
            'videoId' => $id,
            'rank' => $rank,
          ));
          $rank = $rank + 1;
        }
      }

      $tmp = $this->playlist->updatePlaylist($dataToSend, $videosToReorder);
      if ($tmp['status'] === 'OK') {
        echo json_encode(array(
          'status' => 'OK',
          'title' => 'Spilleliste endret',
          'message' => $dataToSend['title'] . " ble endret",
        ));
      } else {
        echo json_encode($tmp);
      }
    } else {
      echo json_encode($valid);
    }
  }

  /**
   * @param array $formData is all the data from the post request.
   * @param array $file is the file information from the post request.
   * This data is passed to the Video class' updateVideo and
   * prepareDataForInsertion function.
   */
  private function changeVideo($formData, $file, $username)
  {
    $valid = Validator::validateVideoSubmit($formData, $file);

    if ($valid['status'] === 'OK') {
      $dataToSend = $this->video->prepareDataForInsertion(
        $formData,
        $file,
        $username,
      );
      $tmp = $this->video->updateVideo($dataToSend);
      echo json_encode(array(
        'status' => 'OK',
        'title' => 'Video endret',
        "message" => $dataToSend['name'] . " ble endret",
      ));
    } else {
      echo json_encode($valid);
    }
  }
}

?>
