<?php

session_start();

header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: *');
if (isset($_GET['subtitles'])) {
  header('Content-Type: text/vtt; charset=utf-8');
} else {
  header('Content-Type: application/json; charset=utf-8');
}

header(
  'Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Allow-Credentials, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With',
);

require_once 'classes/Validator.php';
require_once 'classes/DB.php';
require_once 'classes/User.php';
require_once 'classes/Admin.php';
require_once 'classes/Teacher.php';
require_once 'classes/Student.php';
require_once 'classes/Anon.php';
require_once 'classes/Video.php';
require_once 'classes/Playlist.php';

$db = DB::getDBConnection();

if ($db != null) {
  $user = new User($db);

  $valid = null;

  if (!file_exists('uploadedFiles/tmp.jpg')) {
    @mkdir("uploadedFiles");
  }

  if (isset($_POST['logout'])) {
    $user->logout();
    echo json_encode(array('loggedOut' => true));
  }

  if (isset($_GET['checkSession'])) {
    echo json_encode($_SESSION);
  }

  if (isset($_POST['createUser'])) {
    $valid = Validator::validateCreateUserData($_POST);
    if ($valid['status'] === 'OK') {
      $createUserTest = $user->addUser($_POST);
      echo json_encode($createUserTest);
    } else {
      echo json_encode($valid);
    }
  }
  if (isset($_POST['loginUser'])) {
    $valid = Validator::validateLoginData($_POST['email'], $_POST['password']);
    if ($valid['status'] === 'OK') {
      $loginTest = $user->loginUser($_POST['email'], $_POST['password']);
      echo json_encode($loginTest);
    } else {
      echo json_encode($valid);
    }
  }

  if (isset($_GET['openVideo'])) {
    $video = new Video($db);
    $tmp = $video->getVideos(null, $_GET['openVideo'], null);
    if ($tmp['status'] === 'OK') {
      $updateView = $video->updateVideo(
        array('idToUpdate' => $_GET['openVideo']),
        true,
      );
      if ($updateView['status'] === 'FAIL') {
        echo json_encode($updateView);
      }
      echo json_encode(array(
        'view' => 'openVideo',
        'video' => $tmp['data'],
        'comments' => $tmp['comments'],
      ));
    } else {
      echo json_encode($tmp);
    }
  }

  if (
    isset($_GET['subtitles']) &&
    isset($_GET['user']) &&
    isset($_GET['videoId'])
  ) {
    $tempUser = $_GET['user'];
    $videoId = $_GET['videoId'];
    echo readfile("./uploadedFiles/$tempUser/$videoId/subtitles.vtt");
  }

  if (isset($_GET['getPlaylist'])) {
    $playlist = new Playlist($db);
    $tmp = $playlist->getPlaylists(null, $_GET['getPlaylist']);
    if ($tmp['status'] === 'OK') {
      echo json_encode(array(
        'playlist' => $tmp['playlist'],
        'videos' => $tmp['videos'],
      ));
    } else {
      echo json_encode(array('error' => 'fant ikke spillelisten'));
    }
  }

  if ($user->loggedIn()) {
    $sessionData = $_SESSION['userData'];
    if ($sessionData['userType'] === 'admin') {
      $admin = new Admin($user);
    } elseif ($sessionData['userType'] === 'teacher') {
      if ($sessionData['validated'] === 0) {
        echo json_encode(array(
          'message' =>
            'Du må være validert av admin før du kan gjøre noe her..',
        ));
      } else {
        $video = new Video($db);
        $playlist = new Playlist($db);
        $teacher = new Teacher($video, $playlist);
      }
    } elseif ($sessionData['userType'] === 'student') {
      $video = new Video($db);
      $playlist = new Playlist($db);
      $student = new Student($video, $playlist);
    }

    if (isset($_POST['commentText'])) {
      $dataToSend = array(
        'username' => $_SESSION['userData']['username'],
        'videoId' => $_POST['videoId'],
        'commentText' => $_POST['commentText'],
      );
      $valid = Validator::validateCommentPost($dataToSend);
      if ($valid['status'] === 'OK') {
        $tmp = $video->postComment($dataToSend);
        if ($tmp['status'] === 'OK') {
          echo json_encode(array(
            'status' => 'OK',
            'message' => 'Kommentar lagt til',
          ));
        } else {
          echo json_encode($tmp);
        }
      } else {
        echo json_encode(array('errorMessage' => $valid['errorInfo'][0]));
      }
    }

    if (isset($_POST['rate'])) {
      $dataToSend = array(
        'username' => $_SESSION['userData']['username'],
        'videoId' => $_POST['videoId'],
        'rate' => $_POST['rate'],
      );
      $valid = Validator::validateRate($dataToSend);
      if ($valid['status'] === 'OK') {
        $tmp = $video->rateVideo($dataToSend);
        if ($tmp['status' === 'OK']) {
          echo json_encode(array('message' => 'Vurderingen lagt til'));
        } else {
          echo json_encode($tmp);
        }
      } else {
        echo json_encode(array('errorMessage' => $valid['errorInfo'][0]));
      }
    }
  } else {
    $video = new Video($db);
    $playlist = new Playlist($db);
    $anon = new Anon($video, $playlist);
  }
} else {
  echo json_encode(['message' => 'Klarte ikke å koble til databasen!']);
}

?>
