export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const logout = () => ({
  type: LOGOUT
});

export const login = user => {
  return {
    type: LOGIN,
    user
  };
};

export const REQUEST_SESSION = 'REQUEST_SESSION';
function requestSession(data) {
  if (data != undefined) {
    return {
      type: REQUEST_SESSION,
      user: data.userData,
      fetching: false
    };
  } else {
    return {
      type: REQUEST_SESSION,
      fetching: true
    };
  }
}

export function getSession() {
  return function(dispatch) {
    dispatch(requestSession());

    return fetch('http://localhost:80/index.php?checkSession=1', {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    })
      .then(
        res => res.json(),
        error => console.log(error)
      )
      .then(data => dispatch(requestSession(data)));
  };
}
