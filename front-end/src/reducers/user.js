import { LOGIN, LOGOUT } from '../actions/user';

async function checkSession() {
  const fetch = await fetch('http://localhost:80/index.php?checkSession=1', {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  });
  return fetch;
}

const initialState = {
  user: {
    loggedIn: false,
    userType: ''
  }
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        user: action.user
      };
    case LOGOUT:
      return {
        ...state,
        user: { loggedIn: false }
      };
    case 'SESSION':
      checkSession()
        .then(res => res.json())
        .then(data => ({ ...state, user: data.userData }));
    default:
      return state;
  }
}
