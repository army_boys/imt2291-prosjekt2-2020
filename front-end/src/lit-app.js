import { LitElement, html } from '@polymer/lit-element/';
import styles from './lit-app-styles.css';

import { Router } from '@vaadin/router';

import './components/home-page.js';
import './components/not-found.js';

import './components/forms/login-module.js';
import './components/forms/register-module.js';
import './components/forms/add-video.js';
import './components/forms/create-playlist.js';
import './components/forms/edit-playlist.js';
import './components/forms/edit-video.js';

import './components/open-video.js';
import './components/open-playlist.js';
import './components/sidebars/sidebar-module.js';
import './components/subscriptions.js';
import './components/search-module.js';
import './components/my-videos.js';
import './components/my-playlists.js';
import './components/give-access.js';
import './components/validate-users.js';

// Components

import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from './store.js';
import { logout, login, getSession } from './actions/user';

class LitApp extends connect(store)(LitElement) {
  static get properties() {
    return {
      user: { type: Object }
    };
  }

  constructor() {
    super();
    this.user = {};
  }

  _stateChanged(state) {
    this.user = state.user.user;
  }

  _storeUnsubscribe() {
    this.unsubscribe();
  }

  firstUpdated() {
    const router = new Router(this.shadowRoot.querySelector('#outlet'));
    router.setRoutes([
      { path: '/', component: 'home-page' },
      { path: '/login', component: 'login-module' },
      { path: '/register', component: 'register-module' },
      { path: '/addVideo', component: 'add-video' },
      { path: '/createPlaylist', component: 'create-playlist' },
      { path: '/showVideos', component: 'show-videos' },
      { path: '/showPlaylists', component: 'show-playlists' },
      { path: '/openVideo/:id', component: 'open-video' },
      { path: '/editVideo/:id', component: 'edit-video' },
      { path: '/openPlaylist/:id', component: 'open-playlist' },
      { path: '/editPlaylist/:id', component: 'edit-playlist' },
      { path: '/subscriptions', component: 'student-subscriptions' },
      { path: '/search/:word', component: 'search-module' },
      { path: '/videos', component: 'my-videos' },
      { path: '/playlists', component: 'my-playlists' },
      { path: '/validate', component: 'validate-users' },
      { path: '/giveAccess', component: 'give-access' },
      { path: '(.*)', component: 'not-found' }
    ]);
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user !== undefined) {
          if (data.user.loggedIn) {
            store.dispatch(login(data.user));
            this.user = data.user;
          }
        }
      }
    });
  }

  render() {
    return html`
      <style>
        ${styles}
      </style>
      <div class="app">
        <sidebar-module></sidebar-module>
        <div id="outlet"></div>
      </div>
    `;
  }
}

customElements.define('lit-app', LitApp);
