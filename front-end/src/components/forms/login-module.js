import { LitElement, html } from '@polymer/lit-element/';

import { login } from '../../actions/user';

import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../../store.js';

import { Router } from '@vaadin/router';
import style from './register.css';
class LoginModule extends connect(store)(LitElement) {
  static get properties() {
    return {
      user: { type: Object },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.user = {};
    this.error = '';
  }

  _stateChanged(state) {
    this.user = state.user.user;
  }
  /**
   * Sends a POST request to the back-end with the login information of the user.
   * @param Event -> submit event of the form, with the email and password of the user.
   * If it is successful the user is redirected to the home page with a newly created
   * session in the backend.
   */
  loginUser(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('loginUser', true);
    fetch('http://localhost:80/index.php', {
      method: 'POST',
      body: formData,
      credentials: 'include'
    })
      .then(res => res.json())
      .then(data => {
        if (data.status === 'OK') {
          let userData = {
            ...data,
            loggedIn: true
          };
          this.error = '';
          store.dispatch(login(userData));
          Router.go({ pathname: '/' });
        } else {
          this.error = 'Feil e-post eller passord';
        }
      })
      .catch(err => (this.error = err));
  }
  render() {
    return html`
      <style>
        ${style}
      </style>
      <div class="login">
        <h1>Login</h1>
        <form @submit="${e => this.loginUser(e)}" enctype="multipart/form-data">
          <label for="email">Epost: </label>
          <input autofocus type="email" name="email" required />
          <label for="password">Passord: </label>
          <input type="password" name="password" required />
          <input type="submit" name="loginUser" value="Logg inn" />
        </form>
      </div>
      ${this.error.length > 0
        ? html`
            <p>${this.error}</p>
          `
        : null}
    `;
  }
}

customElements.define('login-module', LoginModule);
