import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import Video from '../../apiHandlers/video.js';
import styles from './register.css';

class AddVideo extends LitElement {
  static get properties() {
    return {
      error: { type: String },
      message: { type: String },
      loading: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.error = '';
    this.message = '';
    this.loading = false;
  }

  /**
   * Sends a post request to the back-end with the formData posted.
   * Including title, thumbnail, description, video file and video subtitles
   * @param Event the submit event with the form data
   * If it is successful he/she is redirected to the home page.
   */
  publishVideo(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('addVideo', true);
    this.loading = true;
    Video.post(formData)
      .then(data => {
        if (data.status === 'OK') {
          this.message;
          setTimeout(() => {
            Router.go({ pathname: '/' });
            this.loading = false;
          }, 2500);
        } else {
          this.loading = false;
          this.error = data.errorMessage;
        }
      })
      .catch(err => {
        this.loading = false;
        this.error = err;
      });
  }

  render() {
    return html`
      <style>
        ${styles}
      </style>
      <div class="formWrapper">
        <h1>Legg Til video</h1>

        <form
          @submit="${e => this.publishVideo(e)}"
          enctype="multipart/form-data"
          method="POST"
        >
          <label for="thumbnail">Thumbnail</label>
          <input type="file" name="thumbnail" accept=".png,.jpeg,.jpg" />

          <label for="name">Tittel</label>
          <input type="text" name="name" />
          <label for="description">Beskrivelse</label>
          <textarea name="description" id="" cols="30" rows="10"></textarea>

          <label for="video">Video file</label>
          <input type="file" name="video" accept=".webm,.mp4" />
          <label for="videoText"
            >Teksting til videoen (må være .vtt fil format)</label
          >
          <input type="file" name="videoText" accept=".vtt" />
          ${this.loading
            ? html`
                <input
                  disabled
                  type="submit"
                  name="addVideo"
                  value="Publiser video"
                />
              `
            : html`
                <input type="submit" name="addVideo" value="Publiser video" />
              `}
        </form>
      </div>
      ${this.message.length > 0
        ? html`
            <div>
              Status:
              <p>${this.message}</p>
            </div>
          `
        : null}
      ${this.error.length > 0
        ? html`
            <div>
              Error:
              <p>${this.error}</p>
            </div>
          `
        : null}
    `;
  }
}

customElements.define('add-video', AddVideo);
