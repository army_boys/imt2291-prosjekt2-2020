import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../../store.js';
import { login, getSession } from '../../actions/user';
import Video from '../../apiHandlers/video.js';
import styles from './editVideo.css';
const fileLocation = 'http://localhost:80/uploadedFiles';

class EditVideo extends connect(store)(LitElement) {
  static get properties() {
    return {
      video: { type: Object },
      error: { type: String },
      message: { type: String },
      user: { type: Object },
      thumbnailSrc: { type: String },
      loading: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.video = {};
    this.error = '';
    this.message = '';
    this.user = {};
    this.thumbnailSrc = '';
    this.loading = false;
  }

  firstUpdated() {
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user.loggedIn) {
          store.dispatch(login(data.user));
          this.user = data.user;
          if (this.user.userType !== 'teacher') {
            Router.go({ pathname: '/' });
          }
        }
      }
    });
  }
  /**
   * Sends a GET request to the back-end with the id of the video.
   * Fills out the fields in the form dependant on the information stored in the database.
   * @param id of the video being edited, accessed through the connectedCallback function
   * as a URL parameter.
   * If it is successful the video property is filled out.
   */
  getVideo(id) {
    this.loading = true;
    Video.getVideoToEditById(id)
      .then(data => {
        if (data.video !== undefined) {
          this.video = data.video;
          if (
            this.video.fileExtension !== null &&
            this.video.fileExtension !== undefined
          ) {
            this.thumbnailSrc =
              `${fileLocation}/${this.video.userId}/${this.video.id}/thumbnail.${this.video.fileExtension}?` +
              new Date().getTime();
          } else {
            this.thumbnailSrc = 'http://localhost:80/img/tmp.png';
          }
          this.loading = false;
        } else {
          this.video = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end with the formData of the edited video.
   * @param Event the submit event of the form, including all edited data.
   * @param id of the video being edited.
   */
  editVideo(e, id) {
    this.loading = true;
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('changeVideo', true);
    Video.post(formData).then(data => {
      if (data.status === 'OK') {
        this.setMessage((this.message = 'Video endret!'));
        this.getVideo(id);
      } else {
        this.setMessage(data.errorMessage, true);
      }
      this.loading = false;
    });
  }

  setMessage(str, err = false) {
    if (err) {
      this.error = str;
    } else {
      this.message = str;
    }
  }

  connectedCallback() {
    let id = this.location.params.id;
    this.getVideo(id);
  }

  render() {
    const { video, loading } = this;
    if (video.name !== undefined) {
      return html`
      <style>
        ${styles}
      </style>
        <div class="editVideo">
          <form
            method="POST"
            @submit="${e => this.editVideo(e, video.id)}"
            enctype="multipart/form-data"
          >
            <input type="hidden" name="id" value="${video.id}" />
            <label for="name">Tittel:</label>
            <input type="text" name="name" value="${video.name}" />
            <label for="description">Beskrivelse</label>
            <textarea name="description" cols="30" rows="10">
${video.description}</textarea
            >
            <label for="thumbnail">Thumbnail</label>
            <input type="file" name="thumbnail" />
            ${loading
          ? null
          : html`
                  ${video.fileExtension !== null &&
              video.fileExtension !== undefined
              ? html`
                        <img width="60px" .src="${this.thumbnailSrc}" />
                      `
              : html`
                        <img width="20px" .src="${this.thumbnailSrc}" />
                      `}
                `}
            <label for="videoText"
              >Teksting til videoen (må være .vtt fil format)</label
            >
            <input type="file" name="videoText" accept=".vtt" />
            <input type="submit" name="changeVideo" value="Lagre endringer" />

            ${this.message.length > 0
          ? html`
                  <p class="success">${this.message}</p>
                `
          : null}
            ${this.error.length > 0
          ? html`
                  <p class="error">${this.error}</p>
                `
          : null}
          </form>
        </div>
      `;
    } else {
      html`
        <h1>404 playlist not found</h1>
      `;
    }
  }
}

customElements.define('edit-video', EditVideo);
