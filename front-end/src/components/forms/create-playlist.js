import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import style from './register.css';
import Playlist from '../../apiHandlers/playlist';
import Video from '../../apiHandlers/video';

const fileLocation = 'http://localhost:80/uploadedFiles';
class CreatePlaylist extends LitElement {
  static get properties() {
    return {
      myVideos: { type: Array },
      error: { type: String },
      message: { type: String }
    };
  }

  constructor() {
    super();
    this.myVideos = [];
    this.error = '';
    this.message = '';
  }

  firstUpdated() {
    this.error = '';
    Video.getMyVideos()
      .then(data => {
        if (data.videos !== undefined) {
          this.myVideos = data.videos;
        } else {
          this.myVideos = [];
          this.message = data.message;
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a post request to the back-end with the formData posted.
   * Including title, description, topic, course, and any videos that are being added into the playlist
   * @param Event the submit event with the form data
   * If it is successful he/she is redirected to the home page.
   */
  addPlaylist(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('addPlaylist', true);
    Playlist.post(formData)
      .then(data => {
        if (data.status === 'OK') {
          this.message;
          setTimeout(() => {
            Router.go({ pathname: '/' });
          }, 2500);
        } else {
          this.error = data.errorMessage;
        }
      })
      .catch(err => (this.error = err));
  }

  render() {
    return html`
      <style>
        ${style}
      </style>
      <h1>Lag spilleliste</h1>
      <form @submit="${e => this.addPlaylist(e)}" enctype="multipart/form-data">
        <label for="title">Tittel</label>
        <input type="text" name="title" />
        <label for="description">Beskrivelse</label>
        <textarea name="description" id="createPlaylistDesc" cols="30" rows="10"></textarea>
        <label for="topic">Emne</label>
        <input type="text" name="topic" />
        <label for="course">Fag</label>
        <input type="text" name="course" />

        <h4>Legg til videoer i spillelisten</h4>
        <div>
          <table>
            <tr>
              <th>Thumbnail</th>
              <th>Tittel</th>
              <th>Beskrivelse</th>
              <th>Seertall</th>
              <th>Tidspunkt laget</th>
              <th>Legg til i spillelisten</th>
            </tr>
            ${this.myVideos.length > 0
              ? this.myVideos.map(
                  (video, i) => html`
                    <tr>
                      <td>
                        <img
                          src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                        />
                      </td>
                      <td>${video.name}</td>
                      <td>${video.description}</td>
                      <td>${video.views}</td>
                      <td>${video.time}</td>
                      <td>
                        <input
                          type="checkbox"
                          name="videosToAdd[]"
                          value="${video.id}"
                          id="${i}"
                        />
                      </td>
                    </tr>
                  `
                )
              : html`
                  <p>Du har ingen videoer</p>
                `}
          </table>
        </div>
        <div class="submit">
          <input type="submit" name="addPlaylist" value="Lag spillelisten" />
        </div>
      </form>
    `;
  }
}

customElements.define('create-playlist', CreatePlaylist);
