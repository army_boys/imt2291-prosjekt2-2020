import { LitElement, html } from '@polymer/lit-element/';

import { login } from '../../actions/user';

import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../../store.js';

import { Router } from '@vaadin/router';

import style from './register.css';

class RegisterModule extends connect(store)(LitElement) {
  static get properties() {
    return {
      user: { type: Object },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.user = {};
    this.error = '';
  }

  _stateChanged(state) {
    this.user = state.user;
  }

  /**
   * Sends a POST request to the back-end with the user information.
   * @param Event -> submit event of the form, with the contact information of the user.
   * If it is successful the user is redirected to the home page with a newly created user and
   * session in the backend.
   */
  createUser(e) {
    e.preventDefault();
    let formData = new FormData(e.target);
    formData.append('createUser', true);
    fetch('http://localhost:80/index.php', {
      method: 'POST',
      body: formData,
      credentials: 'include'
    })
      .then(res => {
        console.log(res);
        return res.json();
      })
      .then(data => {
        console.log(data);
        if (data.status === 'OK') {
          let userData = {
            ...data,
            loggedIn: true
          };
          this.error = '';
          store.dispatch(login(userData));
          Router.go({ pathname: '/' });
        } else {
          this.error = 'Feil e-post eller passord';
        }
      })
      .catch(err => (this.error = err));
  }

  render() {
    return html`
      <style>
        ${style}
      </style>
      <div class="register">
        <h1>Registrer deg her</h1>
        <form
          @submit="${e => this.createUser(e)}"
          enctype="multipart/form-data"
        >
          <div class="userName">
            <label for="username">Brukernavn: </label>
            <input autofocus type="text" name="username" required />
          </div>
          <div class="fullName">
            <label for="fullname">Fullt navn: </label>
            <input autofocus type="text" name="fullname" required />
          </div>
          <label for="email">Epost: </label>
          <input type="email" name="email" required />
          <label for="password01">Passord: </label>
          <input type="password" name="password01" required />
          <label for="password">Gjennta passord: </label>
          <input type="password" name="password02" required />

          <div>
            <label for="userType">Brukertype:</label>
            <div class="userType">
              <p>Student</p>
              <input type="radio" name="userType" value="student" checked />
            </div>
            <div class="userType">
              <p>Lærer</p>
              <input type="radio" name="userType" value="teacher" />
            </div>
            <div class="userType">
              <p>Admin</p>
              <input type="radio" name="userType" value="admin" />
            </div>
          </div>
          <div class="RegisterSubmit">
            <input type="submit" name="createUser" value="Lag bruker" />
          </div>
        </form>
      </div>
      ${this.error.length > 0
        ? html`
            <p>${this.error}</p>
          `
        : null}
    `;
  }
}

customElements.define('register-module', RegisterModule);
