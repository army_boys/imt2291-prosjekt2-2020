import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../../store.js';
import { login, getSession } from '../../actions/user';
import Playlist from '../../apiHandlers/playlist.js';
import style from './register.css';

const fileLocation = 'http://localhost:80/uploadedFiles';

class EditPlaylist extends connect(store)(LitElement) {
  static get properties() {
    return {
      playlist: { type: Object },
      videos: { type: Array },
      otherVideos: { type: Array },
      showMore: { type: Boolean },
      error: { type: String },
      message: { type: String },
      videosToRemove: { type: Array },
      user: { type: Object }
    };
  }

  constructor() {
    super();
    this.playlist = {};
    this.videos = [];
    this.otherVideos = [];
    this.showMore = false;
    this.error = '';
    this.message = '';
    this.videosToRemove = [];
    this.user = {};
  }

  firstUpdated() {
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user.loggedIn) {
          store.dispatch(login(data.user));
          this.user = data.user;
          if (this.user.userType !== 'teacher') {
            Router.go({ pathname: '/' });
          }
        }
      }
    });
  }

  /**
   * Sends a GET request to the back-end with the id of the playlist.
   * Fills out the fields in the form dependant on the information stored in the database.
   * @param id of the playlist being edited, accessed through the connectedCallback function
   * as a URL parameter.
   * If it is successful the playlist, video and other videos property is filled out.
   * Video is for the videos currently in the playlist, other videos is for the other
   * videos not in the playlist.
   */
  getPlaylist(id) {
    Playlist.getPlaylistToEditById(id)
      .then(data => {
        if (data.playlist !== undefined) {
          this.playlist = data.playlist;
          this.videos =
            data.videos !== undefined && data.videos.length > 0
              ? data.videos
              : [];
          this.otherVideos =
            data.otherVideos !== undefined && data.otherVideos.length > 0
              ? data.otherVideos
              : [];
        } else {
          this.playlist = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end with the formData of the edited playlist.
   * @param Event the submit event of the form, including all edited data.
   * @param id of the playlist being edited.
   */
  editPlaylist(e, id) {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('savePlaylist', true);
    for (let i = 0; i < this.videosToRemove.length; i++) {
      formData.append('videosToRemove[]', this.videosToRemove[i]);
    }
    Playlist.post(formData).then(data => {
      if (data.status === 'OK') {
        this.setMessage((this.message = 'Spilleliste endret!'));
        this.getPlaylist(id);
      } else {
        this.setMessage(data.errorMessage, true);
      }
    });
  }

  setMessage(str, err = false) {
    if (err) {
      this.error = str;
    } else {
      this.message = str;
    }
  }

  connectedCallback() {
    let id = this.location.params.id;
    this.getPlaylist(id);
  }

  /**
   * Prepares an array of all videos being removed from the playlist.
   * @param id of the video
   * @param remove boolean, checking the undo button or the remove button is being clicked.
   */
  setVideosToRemove(id, remove = false) {
    let tempArr = this.videosToRemove.slice();
    if (remove) {
      tempArr.includes(id) ? null : tempArr.push(id);
    } else {
      tempArr = tempArr.filter(el => el !== id);
    }
    this.videosToRemove = tempArr;
  }

  render() {
    const { playlist, videos, otherVideos } = this;
    if (playlist.title !== undefined) {
      return html`
        <style>
          ${style}
        </style>
        <div>
          <form
            method="POST"
            @submit="${e => this.editPlaylist(e, playlist.playlistId)}"
            enctype="multipart/form-data"
          >
            <input type="hidden" name="id" value="${playlist.playlistId}" />
            <label for="title">Tittel</label>
            <input type="text" name="title" value="${playlist.title}" />
            <label for="description">Beskrivelse</label>
            <textarea name="description" cols="30" rows="10">
${playlist.description}</textarea
            >
            <label for="topic">Emne</label>
            <input type="text" name="topic" value="${playlist.topic}" />
            <label for="course">Fag</label>
            <input type="text" name="course" value="${playlist.course}" />
            <h2>Vidoer i spillelisten</h2>
            ${videos.length > 0
              ? videos.map(
                  video => html`
                    <div>
                      ${this.videosToRemove.includes(video.videoId)
                        ? html`
                            <h4><del>${video.videoName}</del></h4>
                          `
                        : html`
                            <h4>${video.videoName}</h4>
                          `}
                      ${video.fileExtension !== null &&
                      video.fileExtension !== undefined
                        ? html`
                            <img
                              src="${fileLocation}/${video.userId}/${video.videoId}/thumbnail.${video.fileExtension}"
                            />
                          `
                        : html`
                            <img
                              width="20px"
                              src="http://localhost:80/img/tmp.png"
                            />
                          `}
                      <span>${video.views}</span>
                      <span>${video.time}</span>
                      <span>${video.videoRank}</span>
                      <p>${video.videoDescription}</p>
                      <input
                        type="hidden"
                        name="videoIds[]"
                        value="${video.videoId}"
                      />
                      <label for="videoOrder"
                        >Video number (endre for å gjøre om rekkefølgen):
                      </label>
                      <input
                        type="text"
                        name="videoOrder[]"
                        value="${video.videoRank}"
                      />
                      <button
                        @click="${() =>
                          Router.go({
                            pathname: `/openVideo/${video.videoId}`
                          })}"
                      >
                        Åpne video
                      </button>
                      ${this.videosToRemove.includes(video.videoId)
                        ? html`
                            <button
                              type="button"
                              @click="${() =>
                                this.setVideosToRemove(video.videoId)}"
                            >
                              Angre
                            </button>
                          `
                        : html`
                            <button
                              type="button"
                              @click="${() =>
                                this.setVideosToRemove(video.videoId, true)}"
                            >
                              Fjern fra spillelisten
                            </button>
                          `}
                    </div>
                  `
                )
              : html`
                  <p>Det er ingen videoer i spillelisten..</p>
                `}
            <h4>Legg til videoer i spillelisten</h4>
            ${otherVideos.length > 0
              ? otherVideos.map(
                  (video, i) =>
                    html`
                      <div>
                        <h4>${video.name}</h4>
                        ${video.fileExtension !== null &&
                        video.fileExtension !== undefined
                          ? html`
                              <img
                                src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                              />
                            `
                          : html`
                              <img
                                width="20px"
                                src="http://localhost:80/img/tmp.png"
                              />
                            `}
                        <span>${video.views}</span>
                        <span>${video.time}</span>
                        <span>${video.rank}</span>
                        <p>${video.description}</p>
                        <div class="addVideo">
                          <h5>Legg til videoen i spillelisten?</h5>
                          <input
                            type="checkbox"
                            name="videosToAdd[]"
                            value="${video.id}"
                            id="${i}"
                          />
                        </div>

                        <button
                          @click="${() =>
                            Router.go({
                              pathname: `/openVideo/${video.id}`
                            })}"
                        >
                          Åpne video
                        </button>
                      </div>
                    `
                )
              : html`
                  <p>Det finnes ikke noen andre videoer</p>
                `}
            <input type="submit" name="savePlaylist" value="Lagre endringer" />
            ${this.message.length > 0
              ? html`
                  <p class="success">${this.message}</p>
                `
              : null}
            ${this.error.length > 0
              ? html`
                  <p class="error">${this.error}</p>
                `
              : null}
          </form>
        </div>
      `;
    } else {
      html`
        <h1>404 playlist not found</h1>
      `;
    }
  }
}

customElements.define('edit-playlist', EditPlaylist);
