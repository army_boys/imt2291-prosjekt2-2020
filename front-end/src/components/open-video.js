import { LitElement, html } from '@polymer/lit-element/';
import './video-module.js';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import { login, getSession } from '../actions/user';
import Video from '../apiHandlers/video.js';
import style from './openVideo.css';

class OpenVideo extends connect(store)(LitElement) {
  static get properties() {
    return {
      video: { type: Object },
      comments: { type: Array },
      message: { type: String },
      error: { type: String },
      comment: { type: String },
      showMore: { type: Boolean },
      user: { type: Object }
    };
  }

  constructor() {
    super();
    this.video = {};
    this.comments = [];
    this.message = '';
    this.error = '';
    this.comment = '';
    this.showMore = false;
    this.user = {};
  }

  firstUpdated() {
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user !== undefined) {
          if (data.user.loggedIn) {
            store.dispatch(login(data.user));
            this.user = data.user;
          }
        }
      }
    });
  }

  __storeUnsubscribe() {
    this.unsubscribe();
  }
  /**
   * Sends a GET request to the back-end to fetch the video being opened
   * @param Id -> id of the video being opened, gotten from the URL parameters
   * If it is successful the video property is set to the response data.
   */
  getVideo(id) {
    this.error = '';
    Video.getVideoWithId(id)
      .then(data => {
        if (data.video !== undefined) {
          this.video = data.video;
          this.comments =
            data.comments !== undefined && data.comments.length > 0
              ? data.comments
              : [];
        } else {
          this.video = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end with the rating of the video and it's id.
   * @param Event -> the submit event of the rating form
   * If it is successful a message will show to confirm this,
   * if it's not successful an error message will show instead
   */
  rateVideo(e, id) {
    this.error = '';
    this.message = '';
    e.preventDefault();
    const formData = new FormData(e.target);
    if (this.user.loggedIn) {
      Video.post(formData)
        .then(data => {
          if (data.status === 'OK') {
            this.setMessage('Vurdering lagt til!');
            this.getVideo(id);
          } else {
            this.setMessage(data.errorMessage, true);
          }
        })
        .catch(err => this.setMessage(err, true));
    } else {
      Router.go({ pathname: '/login' });
    }
  }
  /**
   * Sends a POST request to the back-end with the comment submitted in the form
   * @param Event -> the submit event of the comment form
   * @param Id -> the id of the video being refreshed
   * If it is successful a message will show to confirm this,
   * if it's not successful an error message will show instead.
   * On success the getVideo function is run again, to refresh the comments
   */
  commentOnVideo(e, id) {
    this.error = '';
    this.message = '';
    e.preventDefault();
    const formData = new FormData(e.target);
    if (this.user.loggedIn) {
      Video.post(formData)
        .then(data => {
          if (data.status === 'OK') {
            this.setMessage('Kommentar lag til!');
            this.getVideo(id);
          } else {
            this.setMessage(data.errorMessage, true);
          }
        })
        .catch(err => this.setMessage(err, true));
    } else {
      Router.go({ pathname: '/login' });
    }
  }

  connectedCallback() {
    let id = this.location.params.id;
    this.getVideo(id);
  }

  setMessage(str, err = false) {
    if (err === false) {
      this.message = str;
      this.comment = '';
    } else {
      this.error = str;
    }
    setTimeout(() => {
      this.error = '';
      this.message = '';
    }, 3000);
  }

  render() {
    const { video, comments } = this;
    let displayComments = [];
    if (comments !== undefined) {
      displayComments = this.showMore ? comments.slice() : comments.slice(0, 5);
    }
    if (video.name !== undefined) {
      return html`
        <style>
          ${style}
        </style>
        <div class="openVideo">
          <h2>${video.name}</h2>
          <video-module
            path="${video.videoMime !== null
              ? `${video.userId}/${video.id}/video.${video.videoMime}`
              : null}"
            videoType="${video.videoMime}"
            vttFile="${video.subtitles === '1'
              ? `${video.userId}/${video.id}/subtitles.vtt`
              : null}"
            user="${video.userId}"
            videoId="${video.id}"
          ></video-module>
          <div class="videoInfo">
            <p>${video.description}</p>
            <span>Dato: ${video.time}</span>
            <span>Seertall: ${video.views}</span>
            <span>Lærer: ${video.userId}</span>
            <span>Rating: ${Video.getRoundedRating(video.rating)}</span>
            ${this.user.userType === 'teacher' &&
            this.user.username === video.userId
              ? html`
                  <button
                    class="changeVideo"
                    @click="${() =>
                      Router.go({ pathname: `/editVideo/${video.id}` })}"
                  >
                    Endre videoen?
                  </button>
                `
              : null}
          </div>

          <form
            id="rate"
            @submit="${e => this.rateVideo(e, video.id)}"
            method="POST"
          >
            <input type="hidden" name="videoId" value="${video.id}" />
            <label for="rate">Rate videoen:</label>
            <div class="rate">
              <input type="radio" id="star5" name="rate" value="5" />
              <label for="star5" title="text">5 stars</label>
              <input type="radio" id="star4" name="rate" value="4" />
              <label for="star4" title="text">4 stars</label>
              <input type="radio" id="star3" name="rate" value="3" />
              <label for="star3" title="text">3 stars</label>
              <input type="radio" id="star2" name="rate" value="2" />
              <label for="star2" title="text">2 stars</label>
              <input type="radio" id="star1" name="rate" value="1" />
              <label for="star1" title="text">1 star</label>
            </div>
            <input type="submit" id="rateVideo" value="Rate video" />
          </form>
          <form
            id="comment"
            @submit="${e => this.commentOnVideo(e, video.id)}"
            method="POST"
          >
            <input type="hidden" name="videoId" value="${video.id}" />
            <label for="comment">Kommenter på videoen</label>
            <input
              type="text"
              name="commentText"
              .value="${this.comment}"
              @change="${e => (this.comment = e.target.value)}"
            />
            <input type="submit" id="sendComment" value="Kommenter" />
          </form>
          ${this.message.length > 0
            ? html`
                <p class="success">${this.message}</p>
              `
            : null}
          ${this.error.length > 0
            ? html`
                <p class="error">${this.error}</p>
              `
            : null}
          <div></div>
          ${displayComments !== undefined && displayComments.length > 0
            ? html`
                ${displayComments.map(
                  c => html`
                    <div class="comment">
                      <h4>${c.userId}</h4>
                      <span>${c.comment}</span>
                      <p>Tidspunkt: ${c.time}</p>
                    </div>
                  `
                )}
                <button
                  class="show"
                  @click="${() => (this.showMore = !this.showMore)}"
                >
                  ${this.showMore ? 'Vis mindre' : 'Vis flere'}
                </button>
              `
            : html`
                <div>
                  <h4>Her er det ingen kommentarer</h4>
                </div>
              `}
        </div>
      `;
    } else {
      html`
        <h1>404 video not found</h1>
      `;
    }
  }
}

customElements.define('open-video', OpenVideo);
