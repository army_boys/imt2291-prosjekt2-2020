import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import { login, getSession } from '../actions/user';
import Playlist from '../apiHandlers/playlist.js';
import style from './open-playlist.css';
const fileLocation = 'http://localhost:80/uploadedFiles';

class OpenPlaylist extends connect(store)(LitElement) {
  static get properties() {
    return {
      playlist: { type: Object },
      videos: { type: Array },
      showMore: { type: Boolean },
      user: { type: Object },
      id: { type: String },
      subscribed: { type: Boolean },
      error: { type: String },
      message: { type: String }
    };
  }

  firstUpdated() {
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user !== undefined) {
          if (data.user.loggedIn) {
            store.dispatch(login(data.user));
            this.user = data.user;
            if (this.user.userType === 'student') {
              this.checkIfSubscribed(this.id);
            }
          }
        }
      }
    });
  }
  constructor() {
    super();
    this.playlist = {};
    this.videos = [];
    this.showMore = false;
    this.user = {};
    this.id = '';
    this.subscribed = false;
    this.message = '';
    this.error = '';
  }
  /**
   * Sends a GET request to the back-end to get the playlist being opened
   * @param id of the playlist being opened
   * If it is successful the playlist property is set to the response data,
   * and the videos within being set to the videos property.
   */
  getPlaylist(id) {
    Playlist.getPlaylistWithId(id)
      .then(data => {
        if (data.playlist !== undefined) {
          this.playlist = data.playlist;
          this.videos =
            data.videos !== undefined && data.videos.length > 0
              ? data.videos
              : [];
        } else {
          this.playlist = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  connectedCallback() {
    let id = this.location.params.id;
    this.id = id;
    this.getPlaylist(id);
  }

  /**
   * Sends a POST request to the back-end to subscribe the student to the playlist
   * @param Event -> submit event with the formData of the current playlist being subscribed to
   * If it is successful a success message is sendt if not a error is sendt.
   */
  subscribeToPlaylist(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('subscribe', true);
    Playlist.post(formData).then(data => {
      if (data.status === 'OK') {
        this.setMessage('Du er nå abonnert på denne spillelisten!');
        this.checkIfSubscribed(this.id);
      } else {
        this.setMessage(data.errorMessage, true);
      }
      this.loading = false;
    });
  }

  /**
   * Sends a POST request to the back-end to unsubscribe the student from the current playlist
   * @param Event -> submit event with the formData of the current playlist being unsubscribed to
   * If it is successful a success message is sendt if not a error is sendt.
   */
  unsubscribeToPlaylist(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    formData.append('unsubscribe', true);
    Playlist.post(formData).then(data => {
      if (data.status === 'OK') {
        this.setMessage('Du har avsluttet abonnementet på denne spillelisten!');
        this.checkIfSubscribed(this.id);
      } else {
        this.setMessage('Fikk ikke avsluttet abonnementet', true);
      }
      this.loading = false;
    });
  }

  /**
   * Sends a GET request to the back-end to check if the current playlist is
   * subscribed to by the current user
   * @param Id -> id of the playlist
   * When fetched the subscribed boolean is set, and is based on the
   * content of the response data's content.
   */
  checkIfSubscribed(id) {
    Playlist.getMySubscriptions()
      .then(data => {
        if (data.playlists !== undefined) {
          this.subscribed = data.playlists.some(playlist => playlist.id === id);
        } else {
          this.subscribed = false;
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  setMessage(str, err = false) {
    if (err) {
      this.error = str;
    } else {
      this.message = str;
    }
  }
  render() {
    const { playlist, videos } = this;
    let videosToDisplay = [];
    if (videos.length > 0) {
      videosToDisplay = this.showMore ? videos.slice() : videos.slice(0, 5);
    }
    if (playlist.title !== undefined) {
      return html`
        <style>
          ${style}
        </style>
        <div class="playlistWrapper">
          <h1>${playlist.title}</h1>
          <span>${playlist.userId}</span>
          <span>Fag: ${playlist.course} - Emne:${playlist.topic}</span>
          <p>${playlist.description}</p>
          <h2>Vidoer i spillelisten</h2>
          <div>
            ${videosToDisplay.length > 0
              ? html`
                  ${videosToDisplay.map(
                    video => html`
                      <div
                        class="videoBox"
                        @click="${() =>
                          Router.go({
                            pathname: `/openVideo/${video.videoId}`
                          })}"
                      >
                        ${video.fileExtension !== null &&
                        video.fileExtension !== undefined
                          ? html`
                              <img
                                src="${fileLocation}/${video.userId}/${video.videoId}/thumbnail.${video.fileExtension}"
                              />
                            `
                          : html`
                              <img
                                width="20px"
                                src="http://localhost:80/img/tmp.png"
                              />
                            `}
                        <div class="inner">
                          <h4>${video.videoName}</h4>
                          <span>Seertall: ${video.views}</span>
                          <span>Dato lagt ut: ${video.time}</span>
                          <p>${video.videoDescription}</p>
                        </div>
                      </div>
                    `
                  )}
                  ${videos.length > 5
                    ? html`
                        <button
                          @click="${() => (this.showMore = !this.showMore)}"
                        >
                          ${this.showMore === true ? 'Vis flere' : 'Vis færre'}
                        </button>
                      `
                    : null}
                `
              : html`
                  <p>Det er ingen videoer i spillelisten..</p>
                `}
          </div>
          ${this.user.userType === 'teacher'
            ? html`
                <button
                  class="edit"
                  @click="${() =>
                    Router.go({
                      pathname: `/editPlaylist/${playlist.playlistId}`
                    })}"
                >
                  Endre spillelisten?
                </button>
              `
            : null}
          ${this.user.userType === 'student'
            ? this.subscribed
              ? html`
                  <form
                    method="POST"
                    @submit="${e => this.unsubscribeToPlaylist(e)}"
                  >
                    <input
                      type="hidden"
                      name="playlistId"
                      value="${playlist.playlistId}"
                    />
                    <input
                      class="edit"
                      type="submit"
                      name="unsubscribe"
                      value="Avslutt abonnement"
                    />
                  </form>
                `
              : html`
                  <form
                    method="POST"
                    @submit="${e => this.subscribeToPlaylist(e)}"
                  >
                    <input
                      type="hidden"
                      name="playlistId"
                      value="${playlist.playlistId}"
                    />
                    <input
                      class="edit"
                      type="submit"
                      name="subscribe"
                      value="Abonner på spillelisten"
                    />
                  </form>
                `
            : null}
        </div>
        ${this.message.length > 0
          ? html`
              <p class="success">${this.message}</p>
            `
          : null}
        ${this.error.length > 0
          ? html`
              <p class="error">${this.error}</p>
            `
          : null}
      `;
    } else {
      html`
        <h1>404 playlist not found</h1>
      `;
    }
  }
}

customElements.define('open-playlist', OpenPlaylist);
