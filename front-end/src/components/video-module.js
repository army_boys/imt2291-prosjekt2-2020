import { LitElement, html } from '@polymer/lit-element/';
const fileLocation = 'http://localhost:80/uploadedFiles';
import style from './videoModule.css';
class VideoModule extends LitElement {
  static get properties() {
    return {
      path: { type: String },
      videoType: { type: String },
      width: { type: String },
      vttFile: { type: String },
      subtitle: { type: Object },
      user: { type: String },
      videoId: { type: String }
    };
  }

  constructor() {
    super();
    this.path = 'testVideo.mp4';
    this.user = '';
    this.videoId = '';
    this.videoType = 'mp4';
    this.vttFile = null;
    this.width = '100%';
    this.subtitle = {};
  }

  firstUpdated() {
    if (this.vttFile !== null && this.vttFile !== '') {
      fetch(
        `http://localhost:80/index.php?subtitles=1&user=${this.user}&videoId=${this.videoId}`,
        {
          method: 'GET',
          credentials: 'include',
          headers: {
            'Content-Type': 'text/vtt'
          }
        }
      )
        .then(data => data.text())
        .then(res => {
          this.subtitle.text = res;
          this.subtitle.type = 'text/vtt';
          this.subtitle.lang = 'en';
          this.subtitle.label = 'English';
          this.init();
        });
    }
  }

  /**
   * Selects the video and adds subtitles if there are any.
   * The subtitles are both added to the video and to the cueHolder.
   * The cueHolder makes it possible for the user to click the
   * text of the subtitles and jump to that part of the video
   */
  init() {
    let video = this.shadowRoot.querySelector('video');
    let cueHolder = this.shadowRoot.querySelector('#cueHolder');
    let track = video.addTextTrack(
      'subtitles',
      this.subtitle.label,
      this.subtitle.lang
    );
    track.mode = 'showing';
    let cueEl = null;
    let textNode = null;
    this.vttParser(this.subtitle.text).map(cue => {
      cueEl = document.createElement('LI');
      textNode = document.createTextNode(cue.text);
      if (cueEl.addEventListener) {
        cueEl.addEventListener(
          'click',
          () => this.jumpToTimestamp(cue.startTime),
          false
        );
      } else {
        if (cueEl.attachEvent) {
          cueEl.attachEvent('click', () => this.jumpToTimestamp(cue.startTime));
        }
      }
      cueEl.appendChild(textNode);
      track.addCue(cue);
      cueHolder.appendChild(cueEl);
      cueEl = null;
      textNode = null;
    });
  }
  /**
   * Takes the vtt file and parses it into VTTCue objects.
   */
  vttParser(vtt) {
    var lines = vtt
      .trim()
      .replace('\r\n', '\n')
      .split(/[\r\n]/)
      .map(function(line) {
        return line.trim();
      });
    var cues = [];
    var start = null;
    var end = null;
    var payload = null;
    for (var i = 0; i < lines.length; i++) {
      if (lines[i].indexOf('-->') >= 0) {
        var splitted = lines[i].split(/[ \t]+-->[ \t]+/);
        if (splitted.length != 2) {
          throw 'Error when splitting "-->": ' + lines[i];
        }
        start = this.parseTimestamp(splitted[0]);
        end = this.parseTimestamp(splitted[1]);
      } else if (lines[i] == '') {
        if (start && end && payload) {
          let cue = new VTTCue(start, end, payload);
          cues.push(cue);
          start = null;
          end = null;
          payload = null;
        }
      } else if (start && end) {
        if (payload == null) {
          payload = lines[i];
        } else {
          payload += '\n' + lines[i];
        }
      }
    }
    if (start && end) {
      let cue = new VTTCue(start, end, payload);
      cues.push(cue);
    }
    return cues;
  }

  /**
   * Gets the timestamp from the vttParser function and makes it readable by the video,
   * to both display it in the correct part of the video, and make the cueCards clickable.
   */
  parseTimestamp(s) {
    let match = s.match(
      /^(?:([0-9]+):)?([0-5][0-9]):([0-5][0-9](?:[.,][0-9]{0,3})?)/
    );
    if (match == null) {
      throw 'Invalid timestamp format: ' + s;
    }
    let hours = parseInt(match[1] || '0', 10);
    let minutes = parseInt(match[2], 10);
    let seconds = parseFloat(match[3].replace(',', '.'));
    return seconds + 60 * minutes + 60 * 60 * hours;
  }

  jumpToTimestamp(time) {
    let video = this.shadowRoot.querySelector('video');
    video.currentTime = time;
  }

  setPlaybackSpeed(rate) {
    let video = this.shadowRoot.querySelector('video');
    video.playbackRate = rate;
  }

  render() {
    return html`
      <style>
        ${style}
      </style>
      <video
        width="${this.width}"
        src="${fileLocation}/${this.path}"
        type="${this.videoType}"
        controls
      ></video>
      <div class="videoControls">
        <p>Sett video hastighet:</p>
        <button @click="${() => this.setPlaybackSpeed(0.5)}">.5x</button>
        <button @click="${() => this.setPlaybackSpeed(1)}">1x</button>
        <button @click="${() => this.setPlaybackSpeed(1.25)}">1.25x</button>
        <button @click="${() => this.setPlaybackSpeed(1.5)}">1.5x</button>
        <button @click="${() => this.setPlaybackSpeed(2.0)}">2x</button>
      </div>
      <div class="cues">
        <div class="cueTitle"><h4>Stikkord</h4></div>
        <ul id="cueHolder"></ul>
      </div>
    `;
  }
}

customElements.define('video-module', VideoModule);
