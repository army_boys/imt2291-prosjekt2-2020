import { LitElement, html } from '@polymer/lit-element/';
import styles from '../../lit-app-styles.css';

import { Router } from '@vaadin/router';

import './sidebarStudent.js';

// Components

import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../../store.js';
import { logout, login, getSession } from '../../actions/user';

class SidebarModule extends connect(store)(LitElement) {
  static get properties() {
    return {
      user: { type: Object },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.user = {};
    this.error = '';
  }

  _stateChanged(state) {
    this.user = state.user.user;
    this.error = '';
  }

  _storeUnsubscribe() {
    this.unsubscribe();
  }

  firstUpdated() {
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user !== undefined) {
          if (data.user.loggedIn) {
            store.dispatch(login(data.user));
            this.user = data.user;
          }
        }
      }
    });
  }

  /**
   * Sends a post request to the back-end where the user is logged out and session deleted.
   * In the front-end he/she is redirected to the home page.
   */
  logoutUser() {
    const formData = new FormData();
    formData.append('logout', true);
    fetch('http://localhost:80/index.php', {
      method: 'POST',
      body: formData,
      credentials: 'include'
    })
      .then(res => {
        if (res.status === 200) {
          store.dispatch(logout());
          Router.go({ pathname: '/' });
        }
      })
      .catch(err => (this.error = err));
  }

  /**
   * Redirects the user to the search page, with their search word
   * @param Event e selecting the form with the input field for the search.
   */
  search(e) {
    e.preventDefault();
    let searchWord = e.target.searchWord.value.replace(/\s/g, '+');
    if (searchWord.length > 2) {
      Router.go({ pathname: `/search/${searchWord}` });
    } else {
      this.error = 'Søkeordet ditt må være lengre enn to bokstaver';
    }
  }

  render() {
    return html`
      <style>
        ${styles}
      </style>
      <div class="sidebar">
        <div class="sidebarContainer">
          <a href="/">Hjem</a>
          <form @submit="${e => this.search(e)}" enctype="multipart/form-data">
            <input
              class="videoSearch"
              type="text"
              name="searchWord"
              placeholder="søk etter videoer, spillelister"
            />
            <input
              type="submit"
              name="search"
              class="searchButton"
              value="søk"
            />
            ${this.error.length > 0
              ? html`
                  <div class="error">${this.error}</div>
                `
              : null}
          </form>
          ${this.user.loggedIn
            ? this.user.userType === 'student'
              ? html`
                  <div class="sideBarContent">
                    <student-sidebar></student-sidebar>
                    <a
                      class="logout"
                      href="/"
                      @click="${() => this.logoutUser()}"
                      >Logg out</a
                    >
                  </div>
                `
              : this.user.userType === 'teacher'
              ? html`
                  <div class="sideBarContent">
                    <a href="/addVideo">Ny video</a>
                    <a href="/createPlaylist" class="spilleliste"
                      >Ny spilleliste</a
                    >
                    <a href="/videos" class="videos">Vis videoer</a>
                    <a href="/playlists" class="playlists">Vis spillelister</a>
                    <a
                      class="logout"
                      href="/"
                      @click="${() => this.logoutUser()}"
                      >Logg out</a
                    >
                  </div>
                `
              : html`
                  <div class="sideBarContent">
                    <a href="/validate">Valider brukere</a>
                    <a href="/giveAccess" class="access">Gi tilgang</a>
                    <a
                      class="logout"
                      href="/"
                      @click="${() => this.logoutUser()}"
                      >Logg out</a
                    >
                  </div>
                `
            : html`
                <div class="loginAndRegister">
                  <a href="/login">Logg inn</a>
                  <a href="/register">Registrer deg</a>
                </div>
              `}
        </div>
      </div>
    `;
  }
}

customElements.define('sidebar-module', SidebarModule);
