import { LitElement, html } from '@polymer/lit-element';

import Playlist from '../../apiHandlers/playlist.js';

import styles from './sidebarStudent.css';

class SidebarStudent extends LitElement {
  static get properties() {
    return {
      mySubscriptions: { type: Array }
    };
  }

  constructor() {
    super();
    this.mySubscriptions = [];
  }

  firstUpdated() {
    this.fetchMySubscriptions();
  }

  /**
   * Sends a GET request to the back-end to fetch the playlists this student is subscribed to.
   * Sets the mySubscription array to be the resulting data if any.
   */
  fetchMySubscriptions() {
    let prevId = '';
    let tempArr = [];
    Playlist.getMySubscriptions().then(data => {
      if (data.playlists !== undefined) {
        data.playlists.forEach(playlist => {
          if (prevId !== playlist.id) {
            tempArr.push({ id: playlist.id, title: playlist.title });
          }
          prevId = playlist.id;
        });
        this.mySubscriptions = tempArr;
      } else {
        this.mySubscriptions = [];
      }
    });
  }

  render() {
    return html`
      <style>
        ${styles}
      </style>
      <a href="/subscriptions" class="spilleliste">Mine spillelister</a>
      <div class="mySubs">
        ${this.mySubscriptions.length > 0
          ? html`
              ${this.mySubscriptions.map(
                playlist =>
                  html`
                    <a href="/openPlaylist/${playlist.id}">${playlist.title}</a>
                  `
              )}
            `
          : null}
      </div>
    `;
  }
}

customElements.define('student-sidebar', SidebarStudent);
