import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import styles from './myVideos.css';
import Video from '../apiHandlers/video.js';
import style from './my-videos.css';

const fileLocation = 'http://localhost:80/uploadedFiles';

class MyVideos extends connect(store)(LitElement) {
  static get properties() {
    return {
      error: { type: String },
      user: { type: Object },
      myVideos: { type: Array },
      myPlaylists: { type: Array }
    };
  }

  _stateChanged(state) {
    this.user = state.user.user;
  }

  constructor() {
    super();
    this.error = '';
    this.user = {};
    this.myVideos = [];
  }

  firstUpdated() {
    this.fetchMyVideos();
  }

  /**
   * Sends a GET request to the back-end to get the videos added by the current user
   * If it is successful the myVideos property is set to the response data
   */
  fetchMyVideos() {
    this.error = '';
    Video.getMyVideos()
      .then(data => {
        if (data.videos !== undefined) {
          this.myVideos = data.videos;
        } else {
          this.myVideos = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  render() {
    return html`
      <style>
        ${style}
      </style>
      ${this.user.validated === '1'
        ? html`
            <div class="myVideos">
              <h2>Mine videoer</h2>
              <div class="wrapper">
                <div class="placeWrapper">
                  ${this.myVideos.length > 0
                    ? this.myVideos.map(
                        video => html`
                          <div
                            class="videoWrapper"
                            @click="${() =>
                              Router.go({
                                pathname: `/openVideo/${video.id}`
                              })}"
                          >
                            ${video.fileExtension !== null &&
                            video.fileExtension !== undefined
                              ? html`
                                  <img
                                    src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                                  />
                                `
                              : html`
                                  <img src="http://localhost:80/img/tmp.png" />
                                `}
                            <div class="myVideosInfo">
                              <h4>${video.name}</h4>

                              <span>${video.views}</span>
                              <span>${video.time}</span>
                            </div>
                          </div>
                        `
                      )
                    : html`
                        <p>Du har ingen videoer ennå</p>
                      `}
                </div>
              </div>
              <div class="addVideo">
                <a class="addVideo" href="/addVideo">Last opp en video her</a>
              </div>
            </div>
          `
        : html`
            <p>
              Du må vente til du er validert av en administrator før du kan
              begynne..
            </p>
          `}
    `;
  }
}

customElements.define('my-videos', MyVideos);
