import { LitElement, html } from '@polymer/lit-element/';

import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';

import './views/student-view.js';
import './views/teacher-view.js';
import './views/admin-view.js';
import './views/anon-view.js';

class HomePage extends connect(store)(LitElement) {
  static get properties() {
    return {
      user: { type: Object }
    };
  }

  constructor() {
    super();
    this.user = {};
  }

  _stateChanged(state) {
    this.user = state.user.user;
  }

  render() {
    const { user } = this;
    if (user.loggedIn) {
      if (user.userType === 'student') {
        return html`

          <student-view></student-view>
        `;
      } else if (user.userType === 'admin') {
        return html`
          <h1>Velkommen adminbruker, ${user.username}!</h1>
          <admin-view></admin-view>
        `;
      } else if (user.userType === 'teacher') {
        return html`
          <teacher-view></teacher-view>
        `;
      }
    } else {
      return html`
        <anon-view></anon-view>
      `;
    }
  }
}

customElements.define('home-page', HomePage);
