import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../../store.js';
import Video from '../../apiHandlers/video.js';
import Playlist from '../../apiHandlers/playlist.js';
import '../video-module.js';
import style from './teacher.css';
const fileLocation = 'http://localhost:80/uploadedFiles';
class TeacherView extends connect(store)(LitElement) {
  static get properties() {
    return {
      error: { type: String },
      user: { type: Object },
      myVideos: { type: Array },
      myPlaylists: { type: Array }
    };
  }

  _stateChanged(state) {
    this.user = state.user.user;
  }

  constructor() {
    super();
    this.error = '';
    this.user = {};
    this.myVideos = [];
    this.myPlaylists = [];
  }

  firstUpdated() {
    this.fetchMyVideos();
    this.fetchMyPlaylists();
  }

  /**
   * Sends a GET request to the back-end to get the videos added by the current user
   * If it is successful the myVideos property is set to the response data
   */
  fetchMyVideos() {
    this.error = '';
    Video.getMyVideos()
      .then(data => {
        if (data.videos !== undefined) {
          this.myVideos = data.videos;
        } else {
          this.myVideos = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a GET request to the back-end to get the playlists added by the current user
   * If it is successful the myPlaylists property is set to the response data
   */
  fetchMyPlaylists() {
    this.error = '';
    Playlist.getMyPlaylists()
      .then(data => {
        if (data.playlists !== undefined) {
          this.myPlaylists = data.playlists;
        } else {
          this.myPlaylists = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  render() {
    return html`
      <style>
        ${style}
      </style>
      ${this.user.validated === '1'
        ? html`
            <div class="videos">
              <div class="myVideos">
                <h2>Mine videoer</h2>
                ${this.myVideos.length > 0
                  ? this.myVideos.map(
                      video => html`
                        <div class="cover"
                          @click="${() =>
                            Router.go({ pathname: `/openVideo/${video.id}` })}"
                        >
                        ${
                          video.fileExtension !== null &&
                          video.fileExtension !== undefined
                            ? html`
                                <img
                                  src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                                />
                              `
                            : html`
                                <img src="http://localhost:80/img/tmp.png" />
                              `
                        }
                        <div class="coverInner">
                          <h4>${video.name}</h4>
                          <span>${video.views}</span>
                          <span>${video.time}</span>
                        </div>        
                      </div>
                    </div>
                      `
                    )
                  : html`
                      <p>Du har ingen videoer ennå</p>
                    `}
              </div>
              <a class="addVideo" href="/addVideo">Last opp en video her</a>
              <div class="myPlaylist">
                <h2>Mine spillelister</h2>
                ${this.myPlaylists.length > 0
                  ? this.myPlaylists.map(
                      playlist => html`
                        <div
                          class="playlistCover"
                          @click="${() =>
                            Router.go({
                              pathname: `/openPlaylist/${playlist.id}`
                            })}"
                        >
                          ${playlist.fileExtension !== undefined &&
                          playlist.fileExtension !== null
                            ? html`
                                <img
                                  src="${fileLocation}/${playlist.userId}/${playlist.videoId}/thumbnail.${playlist.fileExtension}"
                                />
                              `
                            : html`
                                <img src="http://localhost:80/img/tmp.png" />
                              `}
                          <div class="playlistInner">
                            <h4>${playlist.title}</h4>
                            <span>${playlist.topic}</span>
                            <span>${playlist.course}</span>
                          </div>
                        </div>
                      `
                    )
                  : html`
                      <p>Du har ingen spillelister ennå</p>
                    `}
              </div>
              <a class="playlistCreate" href="/createPlaylist"
                >Lag en spilleliste her</a
              >
            </div>
          `
        : html`
            <p>
              Du må vente til du er validert av en administrator før du kan
              begynne..
            </p>
          `}
    `;
  }
}

customElements.define('teacher-view', TeacherView);
