import { LitElement, html } from '@polymer/lit-element/';
import styles from './anon.css';
import Video from '../../apiHandlers/video.js';
import Playlist from '../../apiHandlers/playlist.js';
import { Router } from '@vaadin/router';
const fileLocation = 'http://localhost:80/uploadedFiles';

class AnonView extends LitElement {
  static get properties() {
    return {
      videos: { type: Array },
      randomVideos: { type: Array },
      playlists: { type: Array },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.videos = [];
    this.randomVideos = [];
    this.playlists = [];
    this.error = '';
  }

  firstUpdated() {
    this.fetchNewVideos();
    this.fetchRandomVideos();
    this.fetchNewPlaylists();
  }

  /**
   * Sends a GET request to the back-end to get the newest videos uploaded to the database.
   * If it is successful the videos property is set to the response data
   */
  fetchNewVideos() {
    this.error = '';
    Video.getVideos(6)
      .then(data => {
        if (data.videos !== undefined) {
          this.videos = data.videos;
        } else {
          this.videos = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a GET request to the back-end to get some videos from the database.
   * If it is successful the videos are randomized and the
   * randomVideos property is set as this data
   */
  fetchRandomVideos() {
    this.error = '';
    Video.getVideos('random')
      .then(data => {
        if (data.videos !== undefined) {
          this.randomVideos = this.randomize(data.videos);
        } else {
          this.randomVideos = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }
  /**
   * Sends a GET request to the back-end to get the newest playlists uploaded to the database.
   * If it is successful the playlists property is set to the response data
   */
  fetchNewPlaylists() {
    this.error = '';
    Playlist.getPlaylists(10)
      .then(data => {
        if (data.playlists !== undefined) {
          this.playlists = data.playlists;
        } else {
          this.playlists = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * @param arr the array to be randomized.
   * Takes an array to randomize the content and return it again.
   * @return arr a randomized array.
   */
  randomize(arr) {
    let currIndex = arr.length,
      temporaryValue,
      randomIndex;
    while (0 !== currIndex) {
      randomIndex = Math.floor(Math.random() * currIndex);
      currIndex -= 1;
      temporaryValue = arr[currIndex];
      arr[currIndex] = arr[randomIndex];
      arr[randomIndex] = temporaryValue;
    }
    return arr;
  }

  render() {
    const { videos, randomVideos, playlists } = this;

    return html`
      <style>
        ${styles}
      </style>
      <h1>Nye videoer</h1>

      <div class="container">
        <div class="videos">
          <div class="newVideos">
          ${
      videos.length > 0
        ? videos.map(
          video => html`
                    <div
                      class="player"
                      @click="${() =>
              Router.go({ pathname: `/openVideo/${video.id}` })}"
                    >
                      ${video.fileExtension !== null &&
              video.fileExtension !== undefined
              ? html`
                            <figure>
                              <img
                                width="60px"
                                src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                                alt="${video.name} thumbnail"
                              />
                            </figure>
                          `
              : html`
                            <figure>
                              <img
                                width="60px"
                                src="http://localhost:80/img/tmp.png"
                                alt="video thumbnail"
                              />
                            </figure>
                          `}
                          <div class="newVideoList">
                    <div class="title">${video.name}</div>
                      <span>${video.views}</span>
                      <span>${video.time}</span>
                      </div>
                    </div>
                  `
        )
        : html`
                  <span>Her er det tomt</span>
                `
      }
          </div>
        </div>

        <!-- border here to seperate the content -->
        <div class="line"></div>

          <div class="randomVideosHeading">
            <h2>Random</h2>
          </div>
          <div class="randomVideos">
            ${
      randomVideos.length > 0
        ? randomVideos.map(
          video => html`
                      <div
                        class="randomPlayer"
                        @click="${() =>
              Router.go({ pathname: `/openVideo/${video.id}` })}"
                      >
                        ${video.fileExtension !== null &&
              video.fileExtension !== undefined
              ? html`
                              <figure>
                                <img
                                  width="60px"
                                  src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                                  alt="${video.name} thumbnail"
                                />
                              </figure>
                            `
              : html`
                              <figure>
                                <img
                                  width="60px"
                                  src="http://localhost:80/img/tmp.png"
                                  alt="video thumbnail"
                                />
                              </figure>
                            `}
                        <div class="randomVideosList">
                          <div class="title">${video.name}</div>
                          <span>${video.views}</span>
                          <span>${video.time}</span>
                        </div>
                      </div>
                    `
        )
        : html`
                    <span>Her er det tomt</span>
                  `
      }
          </div>

          <div class="playlists">
          <h2>Spillelister</h2>
          <div class="playlist">
            ${
      playlists.length > 0
        ? playlists.map(
          (playlist, i) => html`
            <div class="playlistBox"
                          @click="${() =>
              Router.go({
                pathname: `/openPlaylist/${playlist.id}`
              })}"
                        >
                          
                          ${
            playlist.fileExtension !== undefined &&
              playlist.fileExtension !== null
              ? html`
                                  <figure>
                                    <img
                                      .class="${i === 0 ? 'first' : ''}"
                                      width="60px"
                                      src="${fileLocation}/${playlist.userId}/${playlist.videoId}/thumbnail.${playlist.fileExtension}"
                                    />
                                  </figure>
                                `
              : html`
                                  <figure>
                                    <img
                                      class="${i === 0 ? 'first' : ''}"
                                      width="60px"
                                      src="http://localhost:80/img/tmp.png"
                                    />
                                  </figure>
                                `
            }
            <div class="list">
              <h4>${playlist.title}</h4>
              <span>Emne: ${playlist.topic}</span>
              <span>Fag: ${playlist.course}</span>
              <span>Videoer i spillelisten:  </span>
            </div>
                        </div>
                      </div>`
        )
        : html`
                    <span>Her er det tomt</span>
                  `
      }
          </div>
        </div>


        </div>

        
      </div>
    `;
  }
}

customElements.define('anon-view', AnonView);
