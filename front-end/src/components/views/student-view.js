import { LitElement, html } from '@polymer/lit-element/';
import Video from '../../apiHandlers/video.js';
import Playlist from '../../apiHandlers/playlist.js';
import { Router } from '@vaadin/router';
import styles from './student-view.css';
const fileLocation = 'http://localhost:80/uploadedFiles';

class StudentView extends LitElement {
  static get properties() {
    return {
      videos: { type: Array },
      mySubscriptions: { type: Array },
      playlists: { type: Array },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.videos = [];
    this.mySubscriptions = [];
    this.playlists = [];
    this.error = '';
  }

  firstUpdated() {
    this.fetchNewVideos();
    this.fetchMySubscriptions();
    this.fetchNewPlaylists();
  }

  /**
   * Sends a GET request to the back-end to get the newest videos uploaded to the database.
   * If it is successful the videos property is set to the response data
   */
  fetchNewVideos() {
    this.error = '';
    Video.getVideos(6)
      .then(data => {
        if (data.videos !== undefined) {
          this.videos = data.videos;
        } else {
          this.videos = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a GET request to the back-end to get the student's subscriptions.
   * If it is successful the mySubscriptions property is set to the response data
   */
  fetchMySubscriptions() {
    this.error = '';
    Playlist.getMySubscriptions()
      .then(data => {
        if (data.playlists !== undefined) {
          this.mySubscriptions = data.playlists;
        } else {
          this.mySubscriptions = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a GET request to the back-end to get the newest playlists uploaded to the database.
   * If it is successful the playlists property is set to the response data
   */
  fetchNewPlaylists() {
    this.error = '';
    Playlist.getPlaylists(10)
      .then(data => {
        if (data.playlists !== undefined) {
          this.playlists = data.playlists;
        } else {
          this.playlists = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  render() {
    const { videos, playlists, mySubscriptions } = this;
    return html`
      <style>
        ${styles}
      </style>
      <h1>Videoer fra dine abonnementer</h1>
      <div class="student">
        <div class="subscriptions">
          <div class="playlists">
            ${mySubscriptions.length > 0
              ? mySubscriptions.slice(0, 5).map(
                  subscription => html`
            <div class="subscriptionBox"
            @click="${() =>
              Router.go({
                pathname: `/openVideo/${subscription.videoId}`
              })}"
          >
            ${
              subscription.fileExtension !== undefined &&
              subscription.fileExtension !== null
                ? html`
                    <figure>
                      <img
                        width="60px"
                        src="${fileLocation}/${subscription.userId}/${subscription.videoId}/thumbnail.${subscription.fileExtension}"
                      />
                    </figure>
                  `
                : html`
                    <figure>
                      <img width="60px" src="http://localhost:80/img/tmp.png" />
                    </figure>
                  `
            }
          </div>
          <div class="list">
            <h4>${subscription.videoName}</h4>
            <span>Emne: ${subscription.topic}</span>
            <span>Fag: ${subscription.course}</span>
            <span>Utgitt: ${subscription.time}</span>
            <span>Seertall: ${subscription.views}</span>
          </div>
        </div>
            `
                )
              : html`
                  Du har ingen abonnementer ennå!
                `}
          </div>
        </div>

        <div class="newVideoHeading">
          <h1>Nye videoer</h1>
        </div>
        <div class="container">
          <div class="videos">
            <div class="newVideos">
              ${videos.length > 0
                ? videos.map(
                    video => html`
                      <div
                        class="player"
                        @click="${() =>
                          Router.go({ pathname: `/openVideo/${video.id}` })}"
                      >
                        ${video.fileExtension !== null &&
                        video.fileExtension !== undefined
                          ? html`
                              <figure>
                                <img
                                  width="60px"
                                  src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                                  alt="${video.name} thumbnail"
                                />
                              </figure>
                            `
                          : html`
                              <figure>
                                <img
                                  width="60px"
                                  src="http://localhost:80/img/tmp.png"
                                  alt="video thumbnail"
                                />
                              </figure>
                            `}
                        <div class="flex">
                          <div class="title">${video.name}</div>
                          <span>${video.views}</span>
                          <span>${video.time}</span>
                        </div>
                      </div>
                    `
                  )
                : html`
                    <span>Her er det tomt</span>
                  `}
            </div>
          </div>

          <!-- border here to seperate the content -->
          <div class="line"></div>
        </div>

        <div class="playlists">
          <h2>Spillelister</h2>
          <div class="playlist">
            ${playlists.length > 0
              ? playlists.map(
                  (playlist, i) => html`
            <div class="playlistBox"
                          @click="${() =>
                            Router.go({
                              pathname: `/openPlaylist/${playlist.id}`
                            })}"
                        >
                          
                          ${
                            playlist.fileExtension !== undefined &&
                            playlist.fileExtension !== null
                              ? html`
                                  <figure>
                                    <img
                                      .class="${i === 0 ? 'first' : ''}"
                                      width="60px"
                                      src="${fileLocation}/${playlist.userId}/${playlist.videoId}/thumbnail.${playlist.fileExtension}"
                                    />
                                  </figure>
                                `
                              : html`
                                  <figure>
                                    <img
                                      class="${i === 0 ? 'first' : ''}"
                                      width="60px"
                                      src="http://localhost:80/img/tmp.png"
                                    />
                                  </figure>
                                `
                          }
            <div class="list">
              <h4>${playlist.title}</h4>
              <span>Emne: ${playlist.topic}</span>
              <span>Fag: ${playlist.course}</span>
              <span>Videoer i spillelisten:  </span>
            </div>
                        </div>
                      </div>`
                )
              : html`
                  <span>Her er det tomt</span>
                `}
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('student-view', StudentView);
