import { LitElement, html } from '@polymer/lit-element/';
import Admin from '../../apiHandlers/admin.js';

class AdminView extends LitElement {
  static get properties() {
    return {
      usersToValidate: { type: Array },
      allUsers: { type: Array },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.usersToValidate = [];
    this.allUsers = [];
    this.error = '';
  }
  firstUpdated() {
    this.fetchAllUsers();
    this.fetchUsersToValidate();
  }
  /**
   * Sends a GET request to the back-end to fetch all users in the database, except the current user.
   * If it is successful the allUsers property is filled with the response data.
   */
  fetchAllUsers() {
    this.error = '';
    Admin.getAllUsers()
      .then(data => {
        this.allUsers = data.users;
      })
      .catch(err => {
        this.error = err;
      });
  }
  /**
   * Sends a GET request to the back-end to fetch all users waiting to be validated in the database.
   * If it is successful the usersToValidate property is filled with the response data.
   */
  fetchUsersToValidate() {
    this.error = '';
    Admin.getUsersToValidate()
      .then(data => {
        this.usersToValidate = data.users;
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end to with the email address of the user to be validated
   * If it is successful the fetchUsersToValidate function is run again.
   */
  validateUser(email) {
    const formData = new FormData();
    formData.append('changeUser', true);
    formData.append('email', email);
    Admin.post(formData)
      .then(data => {
        if (data.status === 'OK') {
          this.fetchUsersToValidate();
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end to with the data from the form, meaning the email address
   * and the type of user to turn this user into
   * If it is successful the fetchAllUsers function is run again.
   */
  giveUserAccess(e) {
    e.preventDefault();
    let formData = new FormData();
    formData.append('giveAccess', true);
    formData.append('email', e.target.email.value);
    formData.append('type', e.target.type.value);
    Admin.post(formData)
      .then(data => {
        if (data.status === 'OK') {
          this.fetchAllUsers();
        }
      })
      .catch(err => {
        this.error = err;
      });
  }
  /**
   * Defines what kind of users the current user can turn into, excluding the one they currently are.
   */
  defineOtherUserTypes(curr) {
    let array = ['admin', 'teacher', 'student'];
    return array.filter(val => val != curr);
  }
  render() {
    return html`
      <div>
        <h2>Brukere som venter på validering</h2>
        ${this.usersToValidate.length > 0
          ? this.usersToValidate.map(
              i => html`
                <p>Navn: ${i.fullname}</p>
                <p>
                  Validert:
                  ${i.validated === 1
                    ? html`
                        JA
                      `
                    : html`
                        NEI
                      `}
                </p>
                <button @click="${() => this.validateUser(i.email)}">
                  Valider bruker
                </button>
              `
            )
          : html`
              <p>Ingen å vise her</p>
            `}
      </div>
      <h2>Alle brukere</h2>
      ${this.allUsers.length > 0
        ? this.allUsers.map(
            i => html`
              <p>Navn: ${i.fullname}</p>
              <p>Brukertype: ${i.userType}</p>
              <form
                @submit="${e => this.giveUserAccess(e)}"
                enctype="multipart/form-data"
              >
                <input hidden name="email" value="${i.email}" />
                <label for="type">Endre brukertype: </label>
                <select name="type">
                  ${this.defineOtherUserTypes(i.userType).map(
                    type => html`
                      <option>${type}</option>
                    `
                  )}
                </select>
                <input type="submit" name="giveAccess" value="Gjør" />
              </form>
            `
          )
        : html`
            <p>Ingen å vise her</p>
          `}
    `;
  }
}

customElements.define('admin-view', AdminView);
