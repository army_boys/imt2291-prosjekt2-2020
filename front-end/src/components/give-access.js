import { LitElement, html } from '@polymer/lit-element/';
import Admin from '../apiHandlers/admin.js';
class GiveAccess extends LitElement {
  static get properties() {
    return {
      allUsers: { type: Array },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.allUsers = [];
    this.error = '';
  }

  firstUpdated() {
    this.fetchAllUsers();
  }

  /**
   * Sends a GET request to the back-end to fetch all users in the database, except the current user.
   * If it is successful the allUsers property is filled with the response data.
   */
  fetchAllUsers() {
    this.error = '';
    Admin.getAllUsers()
      .then(data => {
        this.allUsers = data.users;
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end to with the data from the form, meaning the email address
   * and the type of user to turn this user into
   * If it is successful the fetchAllUsers function is run again.
   */
  giveUserAccess(e) {
    e.preventDefault();
    let formData = new FormData();
    formData.append('giveAccess', true);
    formData.append('email', e.target.email.value);
    formData.append('type', e.target.type.value);
    Admin.post(formData)
      .then(data => {
        if (data.status === 'OK') {
          this.fetchAllUsers();
        }
      })
      .catch(err => {
        this.error = err;
      });
  }
  /**
   * Defines what kind of users the current user can turn into, excluding the one they currently are.
   */
  defineOtherUserTypes(curr) {
    let array = ['admin', 'teacher', 'student'];
    return array.filter(val => val != curr);
  }

  render() {
    return html`
      <h2>Alle brukere</h2>
      ${this.allUsers.length > 0
        ? this.allUsers.map(
            i => html`
              <p>Navn: ${i.fullname}</p>
              <p>Brukertype: ${i.userType}</p>
              <form
                @submit="${e => this.giveUsersAccess(e)}"
                enctype="multipart/form-data"
              >
                <input hidden name="email" value="${i.email}" />
                <label for="type">Endre brukertype: </label>
                <select name="type">
                  ${this.defineOtherUserTypes(i.userType).map(
                    type => html`
                      <option>${type}</option>
                    `
                  )}
                </select>
                <input type="submit" name="giveAccess" value="Gjør" />
              </form>
            `
          )
        : html`
            <p>Ingen å vise her</p>
          `}
    `;
  }
}

customElements.define('give-access', GiveAccess);
