import { LitElement, html } from '@polymer/lit-element';
import Playlist from '../apiHandlers/playlist.js';
import { Router } from '@vaadin/router';
const fileLocation = 'http://localhost:80/uploadedFiles';

class StudentSubscriptions extends LitElement {
  static get properties() {
    return {
      mySubscriptions: { type: Array }
    };
  }

  constructor() {
    super();
    this.mySubscriptions = [];
  }

  firstUpdated() {
    this.fetchMySubscriptions();
  }

  /**
   * Sends a GET request to the back-end to get the student's subscriptions.
   * If it is successful the mySubscriptions property is set to the response data
   */
  fetchMySubscriptions() {
    Playlist.getMySubscriptions().then(data => {
      if (data.playlists !== undefined) {
        this.mySubscriptions = data.playlists;
      } else {
        this.mySubscriptions = [];
      }
    });
  }

  render() {
    const { mySubscriptions } = this;
    return html`
      <style></style>
      <h1>Videoer fra dine abonnementer</h1>
      <div class="subscriptions">
        <div class="playlists">
          ${mySubscriptions.length > 0
            ? mySubscriptions.map(
                subs => html`
                  ${subs.videoId !== undefined && subs.videoId !== null
                    ? html`
                        <div
                          class="subscriptionBox"
                          @click="${() =>
                            Router.go({
                              pathname: `/openVideo/${subs.videoId}`
                            })}"
                        >
                          ${subs.fileExtension !== undefined &&
                          subs.fileExtension !== null
                            ? html`
                                <figure>
                                  <img
                                    width="60px"
                                    src="${fileLocation}/${subs.userId}/${subs.videoId}/thumbnail.${subs.fileExtension}"
                                  />
                                </figure>
                              `
                            : html`
                                <figure>
                                  <img
                                    width="60px"
                                    src="http://localhost:80/img/tmp.png"
                                  />
                                </figure>
                              `}
                        </div>
                        <div class="list">
                          <h4>${subs.videoName}</h4>
                          <span>Emne: ${subs.topic}</span>
                          <span>Fag: ${subs.course}</span>
                          <span>Utgitt: ${subs.time}</span>
                          <span>Seertall: ${subs.views}</span>
                        </div>
                      `
                    : null}
                `
              )
            : html`
                Du har ingen abonnementer ennå!
              `}
        </div>
      </div>
    `;
  }
}

customElements.define('student-subscriptions', StudentSubscriptions);
