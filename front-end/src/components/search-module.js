import { LitElement, html } from '@polymer/lit-element/';

import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import { login, getSession } from '../actions/user';
import Search from '../apiHandlers/search.js';
const fileLocation = 'http://localhost:80/uploadedFiles';

class SearchModule extends connect(store)(LitElement) {
  static get properties() {
    return {
      videos: { type: Array },
      playlists: { type: Array },
      videoMessage: { type: String },
      playlistMessage: { type: String },
      user: { type: Object }
    };
  }

  constructor() {
    super();
    this.videos = [];
    this.playlists = [];
    this.videoMessage = '';
    this.playlistMessage = '';
    this.user = {};
  }

  firstUpdated() {
    this.unsubscribe = store.dispatch(getSession()).then(data => {
      if (data.fetching === false) {
        if (data.user.loggedIn) {
          store.dispatch(login(data.user));
          this.user = data.user;
        }
      }
    });
  }

  /**
   * Sends a GET request to the back-end with the search word,
   * to fetch all videos and playlists with the word present.
   * @param Word -> the search parameter
   * If it is successful the videos and playlist will be filled with the search results.
   */
  search(word) {
    this.error = '';
    Search.getPlaylistAndVideos(word).then(data => {
      if (data.videos !== undefined && data.videos.length > 0) {
        this.videos = data.videos;
      } else {
        this.videos = [];
      }
      if (data.playlists !== undefined && data.playlists.length > 0) {
        this.playlists = data.playlists;
      } else {
        this.playlists = [];
      }
      this.videoMessage = data.videoMessage;
      this.playlistMessage = data.playlistMessage;
    });
  }

  connectedCallback() {
    let word = this.location.params.word;
    this.search(word);
  }

  render() {
    const { playlists, videos, videoMessage, playlistMessage } = this;
    return html`
      <div>
        <h1>Søk</h1>
        <p>${playlistMessage}</p>
        <p>${videoMessage}</p>
        <div class="playlists">
          ${playlists.length > 0
            ? playlists.map(
                playlist => html`
              <div class="playlistBox"
                    @click="${() =>
                      Router.go({
                        pathname: `/openPlaylist/${playlist.id}`
                      })}"
                  >
                    
                    ${
                      playlist.fileExtension !== undefined &&
                      playlist.fileExtension !== null
                        ? html`
                            <figure>
                              <img
                                width="60px"
                                src="${fileLocation}/${playlist.userId}/${playlist.videoId}/thumbnail.${playlist.fileExtension}"
                              />
                            </figure>
                          `
                        : html`
                            <figure>
                              <img
                                width="60px"
                                src="http://localhost:80/img/tmp.png"
                              />
                            </figure>
                          `
                    }
                  </div>
                  <div class="list">
                    <h4>${playlist.title}</h4>
                    <span>Emne: ${playlist.topic}</span>
                    <span>Fag: ${playlist.course}</span>
                    <span>Videoer i spillelisten:  </span>
                  </div>
                </div>`
              )
            : null}
        </div>
        <div class="videos">
          ${videos.length > 0
            ? videos.map(
                video => html`
                  <div
                    class="player"
                    @click="${() =>
                      Router.go({ pathname: `/openVideo/${video.id}` })}"
                  >
                    ${video.fileExtension !== null &&
                    video.fileExtension !== undefined
                      ? html`
                          <figure>
                            <img
                              width="60px"
                              src="${fileLocation}/${video.userId}/${video.id}/thumbnail.${video.fileExtension}"
                              alt="${video.name} thumbnail"
                            />
                          </figure>
                        `
                      : html`
                          <figure>
                            <img
                              width="60px"
                              src="http://localhost:80/img/tmp.png"
                              alt="video thumbnail"
                            />
                          </figure>
                        `}
                    <span>${video.views}</span>
                    <span>${video.time}</span>
                    <div class="title">${video.name}</div>
                  </div>
                `
              )
            : null}
        </div>
      </div>
    `;
  }
}

customElements.define('search-module', SearchModule);
