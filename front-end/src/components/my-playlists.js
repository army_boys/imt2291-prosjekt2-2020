import { LitElement, html } from '@polymer/lit-element/';
import { Router } from '@vaadin/router';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import styles from './myPlaylist.css';
import Playlist from '../apiHandlers/playlist.js';
const fileLocation = 'http://localhost:80/uploadedFiles';
class MyPlaylists extends connect(store)(LitElement) {
  static get properties() {
    return {
      error: { type: String },
      user: { type: Object },
      myPlaylists: { type: Array }
    };
  }

  _stateChanged(state) {
    this.user = state.user.user;
  }

  constructor() {
    super();
    this.error = '';
    this.user = {};
    this.myPlaylists = [];
  }

  firstUpdated() {
    this.fetchMyPlaylists();
  }

  /**
   * Sends a GET request to the back-end to get the playlists added by the current user
   * If it is successful the myPlaylists property is set to the response data
   */
  fetchMyPlaylists() {
    this.error = '';
    Playlist.getMyPlaylists()
      .then(data => {
        if (data.playlists !== undefined) {
          this.myPlaylists = data.playlists;
        } else {
          this.myPlaylists = [];
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  render() {
    return html`
      ${this.user.validated === '1'
        ? html`
            <style>
              ${styles}
            </style>
            <div class="myPlaylist">
              <h2>Mine spillelister</h2>
              ${this.myPlaylists.length > 0
                ? this.myPlaylists.map(
                    playlist => html`
                      <div
                        class="playlist"
                        @click="${() =>
                          Router.go({
                            pathname: `/openPlaylist/${playlist.id}`
                          })}"
                      >
                        ${playlist.fileExtension !== undefined &&
                        playlist.fileExtension !== null
                          ? html`
                              <img
                                src="${fileLocation}/${playlist.userId}/${playlist.videoId}/thumbnail.${playlist.fileExtension}"
                              />
                            `
                          : html`
                              <img src="http://localhost:80/img/tmp.png" />
                            `}
                        <div class="list">
                          <h4>${playlist.title}</h4>
                          <span>${playlist.topic}</span>
                          <span>${playlist.course}</span>
                        </div>
                      </div>
                    `
                  )
                : html`
                    <p>Du har ingen spillelister ennå</p>
                  `}
              <a href="/createPlaylist">Lag en spilleliste her</a>
            </div>
          `
        : html`
            <p>
              Du må vente til du er validert av en administrator før du kan
              begynne..
            </p>
          `}
    `;
  }
}

customElements.define('my-playlists', MyPlaylists);
