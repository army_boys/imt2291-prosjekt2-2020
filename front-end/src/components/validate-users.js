import { LitElement, html } from '@polymer/lit-element/';
import Admin from '../apiHandlers/admin.js';
class ValidateUsers extends LitElement {
  static get properties() {
    return {
      usersToValidate: { type: Array },
      error: { type: String }
    };
  }

  constructor() {
    super();
    this.usersToValidate = [];
    this.error = '';
  }
  firstUpdated() {
    this.fetchUsersToValidate();
  }

  /**
   * Sends a GET request to the back-end to fetch all users waiting to be validated in the database.
   * If it is successful the usersToValidate property is filled with the response data.
   */
  fetchUsersToValidate() {
    this.error = '';
    Admin.getUsersToValidate()
      .then(data => {
        this.usersToValidate = data.users;
      })
      .catch(err => {
        this.error = err;
      });
  }

  /**
   * Sends a POST request to the back-end to with the email address of the user to be validated
   * If it is successful the fetchUsersToValidate function is run again.
   */
  validateUser(email) {
    const formData = new FormData();
    formData.append('changeUser', true);
    formData.append('email', email);
    Admin.post(formData)
      .then(data => {
        if (data.status === 'OK') {
          this.fetchUsersToValidate();
        }
      })
      .catch(err => {
        this.error = err;
      });
  }

  render() {
    return html`
      <div>
        <h2>Brukere som venter på validering</h2>
        ${this.usersToValidate.length > 0
          ? this.usersToValidate.map(
              i => html`
                <p>Navn: ${i.fullname}</p>
                <p>
                  Validert:
                  ${i.validated === 1
                    ? html`
                        JA
                      `
                    : html`
                        NEI
                      `}
                </p>
                <button @click="${() => this.validateUser(i.email)}">
                  Valider bruker
                </button>
              `
            )
          : html`
              <p>Ingen å vise her</p>
            `}
      </div>
    `;
  }
}

customElements.define('validate-users', ValidateUsers);
