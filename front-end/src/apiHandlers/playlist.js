/* GET REQUESTS FOR PLAYLISTS */
async function getPlaylistToEditById(id) {
  let snapshot = await fetch(
    `http://localhost:80/index.php?editPlaylist=${id}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    }
  ).then(res => res.json());
  return snapshot;
}

async function getPlaylistWithId(id) {
  let snapshot = await fetch(
    `http://localhost:80/index.php?getPlaylist=${id}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    }
  ).then(res => res.json());
  return snapshot;
}

async function getMyPlaylists() {
  let snapshot = await fetch(
    'http://localhost:80/index.php?showYourPlaylists=1',
    {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    }
  ).then(res => res.json());
  return snapshot;
}

async function getPlaylists(limit) {
  let snapshot = await fetch(
    `http://localhost:80/index.php?newPlaylists=${limit}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    }
  ).then(res => res.json());
  return snapshot;
}

async function getMySubscriptions() {
  let snapshot = await fetch(
    'http://localhost:80/index.php?showSubscriptions=1',
    {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    }
  ).then(res => res.json());

  return snapshot;
}

/* POST REQUESTS FOR PLAYLISTS */
async function post(data) {
  let snapshot = await fetch('http://localhost:80/index.php', {
    method: 'POST',
    body: data,
    credentials: 'include'
  }).then(res => res.json());
  return snapshot;
}

export default {
  getPlaylistWithId,
  getPlaylistToEditById,
  getMyPlaylists,
  getPlaylists,
  getMySubscriptions,
  post
};
