/* GET REQUESTS FOR VIDEOS */
async function getVideoWithId(id) {
  let snapshot = await fetch(`http://localhost:80/index.php?openVideo=${id}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  }).then(res => res.json());
  return snapshot;
}

async function getMyVideos() {
  let snapshot = await fetch('http://localhost:80/index.php?showYourVideos=1', {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  }).then(res => res.json());

  return snapshot;
}

async function getVideos(limit) {
  let url = 'http://localhost:80/index.php?newVideos=6';
  if (typeof limit === 'number') {
    url = `http://localhost:80/index.php?newVideos=${limit}`;
  } else {
    let nr = 8;
    url = `http://localhost:80/index.php?randomVideos=${nr}`;
  }
  let snapshot = await fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  }).then(res => res.json());

  return snapshot;
}

async function getVideoToEditById(id) {
  let snapshot = await fetch(`http://localhost:80/index.php?editVideo=${id}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  }).then(res => res.json());
  return snapshot;
}

/* POST REQUESTS FOR VIDEOS */
async function post(data) {
  let snapshot = await fetch('http://localhost:80/index.php', {
    method: 'POST',
    body: data,
    credentials: 'include'
  })
    .then(res => res.json())
    .catch(err => err);
  return snapshot;
}

/* MISC FUNCTIONS */
function getRoundedRating(num) {
  let round = (value, precision) => {
    let multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  };
  return round(num, 1);
}

export default {
  getVideoWithId,
  getMyVideos,
  getVideoToEditById,
  getVideos,
  post,
  getRoundedRating
};
