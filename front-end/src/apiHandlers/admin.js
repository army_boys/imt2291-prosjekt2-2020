/* GET REQUESTS FOR USERS */
async function getAllUsers() {
  let snapshot = await fetch('http://localhost:80/index.php?viewUsers=all', {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  }).then(res => res.json());
  return snapshot;
}

async function getUsersToValidate() {
  let snapshot = await fetch(
    'http://localhost:80/index.php?viewUsers=validate',
    {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      },
      credentials: 'include'
    }
  ).then(res => res.json());
  return snapshot;
}

/* POST REQUESTS FOR USERS */
async function post(data) {
  let snapshot = await fetch('http://localhost:80/index.php', {
    method: 'POST',
    body: data,
    credentials: 'include'
  }).then(res => res.json());
  return snapshot;
}

export default {
  getAllUsers,
  getUsersToValidate,
  post
};
