/* GET REQUESTS FOR SEARCH */
async function getPlaylistAndVideos(word) {
  let snapshot = await fetch(`http://localhost:80/index.php?search=${word}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    },
    credentials: 'include'
  }).then(res => res.json());

  return snapshot;
}

export default {
  getPlaylistAndVideos
};
